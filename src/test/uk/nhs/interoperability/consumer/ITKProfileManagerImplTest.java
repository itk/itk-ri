/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer;

import static org.junit.Assert.*;

import org.junit.Test;

import uk.nhs.interoperability.capabilities.ITKProfileManager;

public class ITKProfileManagerImplTest {
	
	private static final ITKProfileManager PROFILE_MANAGER = new ITKProfileManagerImpl();

	@Test
	public void testGetProfileSupportLevel() {
		assertEquals(ITKProfileManager.NOT_SUPPORTED, PROFILE_MANAGER.getProfileSupportLevel("bob"));
		assertEquals(ITKProfileManager.ACCEPTED, PROFILE_MANAGER.getProfileSupportLevel("urn:nhs:itk:services:201005:transferPatient-v1-0"));
		assertEquals(ITKProfileManager.DEPRECATED, PROFILE_MANAGER.getProfileSupportLevel("urn:nhs:itk:services:201005:createPatient-v1-0"));
	}

}
