/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XPathsTest {

	@Test
	public void testXpathComplitation() {
		assertNotNull(XPaths.SOAP_HEADER_XPATH); 
	}
	
	@Test
	public void testInputSourceXpathEvaluation() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException {
		InputSource is = new InputSource(new StringReader(DomUtilsTest.TEST_XML_LARGE));
		Node root = (Node)XPaths.ROOT_XPATH.evaluate(is, XPathConstants.NODE);
		assertEquals("soap:Envelope", root.getFirstChild().getNodeName());
		is.setCharacterStream(new StringReader(DomUtilsTest.TEST_XML_LARGE));
		Node soapHeader = (Node)XPaths.SOAP_HEADER_XPATH.evaluate(is, XPathConstants.NODE);
		assertNotNull(soapHeader);
		assertEquals("soap:Header", soapHeader.getNodeName());
		String action = XPaths.WSA_ACTION_XPATH.evaluate(root);
		assertEquals("urn:nhs-itk:services:201005:createPatient-v1-0", action);
	}
	
	@Test
	public void testDocXpathEvaluation() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException {
		Document doc = DomUtils.parse(DomUtilsTest.TEST_XML_LARGE);
		Node root = (Node)XPaths.ROOT_XPATH.evaluate(doc, XPathConstants.NODE);
		assertEquals("soap:Envelope", root.getFirstChild().getNodeName());
		Node soapHeader = (Node)XPaths.SOAP_HEADER_XPATH.evaluate(root.getFirstChild(), XPathConstants.NODE);
		assertNotNull(soapHeader);
		assertEquals("soap:Header", soapHeader.getNodeName());
		String action = XPaths.WSA_ACTION_XPATH.evaluate(doc);
		assertEquals("urn:nhs-itk:services:201005:createPatient-v1-0", action);
	}

}
