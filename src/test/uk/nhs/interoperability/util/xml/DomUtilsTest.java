/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util.xml;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.util.Logger;

public class DomUtilsTest {
	
	public static final String TEST_XML = "<root><child1 child1attr=\"child1-attribute\">child 1 text</child1><child2></child2></root>";
	public static final String TEST_XML_LARGE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<soap:Envelope xmlns:itk=\"urn:nhs-itk:ns:201005\"\n"
			+ "    xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n"
			+ "    <soap:Header xmlns:local=\"local-namespace-uri\">\n"
			+ "        <wsa:MessageID>f75c6755-251e-4bd1-95a3-c7a74850ed36</wsa:MessageID>\n"
			+ "        <wsa:Action>urn:nhs-itk:services:201005:createPatient-v1-0</wsa:Action>\n"
			+ "        <wsa:To>http://127.0.0.1:4000/syncsoap</wsa:To>\n"
			+ "        <local:LocalHeaderElement>Local_Data_To_Be_Ignored</local:LocalHeaderElement>\n"
			+ "        <wsa:From>\n"
			+ "            <wsa:Address>http://localhost:4000</wsa:Address>\n"
			+ "        </wsa:From>\n"
			+ "        <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\n"
			+ "            <wsu:Timestamp wsu:Id=\"D6CD5232-14CF-11DF-9423-1F9A910D4703\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n"
			+ "                <wsu:Created>2012-12-04T13:59:02.056Z</wsu:Created>\n"
			+ "                <wsu:Expires>2012-12-04T13:59:02.056Z</wsu:Expires>\n"
			+ "            </wsu:Timestamp>\n"
			+ "            <wsse:UsernameToken>\n"
			+ "                <wsse:Username>TKS Server test</wsse:Username>\n"
			+ "            </wsse:UsernameToken>\n"
			+ "        </wsse:Security>\n"
			+ "    </soap:Header>\n"
			+ "    <soap:Body>\n"
			+ "        <itk:DistributionEnvelope xmlns:hl7v2=\"urn:hl7-org:v2xml\"\n"
			+ "            xmlns:itk=\"urn:nhs-itk:ns:201005\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
			+ "            <itk:header\n"
			+ "                service=\"urn:nhs-itk:services:201005:createPatient-v1-0\" trackingid=\"2D37D9CA-5223-41C7-A159-F33D5A914EB5\">\n"
			+ "                <itk:auditIdentity>\n"
			+ "                    <itk:id uri=\"urn:nhs-uk:identity:cfh:nic:lab:itk-demonstrator\"/>\n"
			+ "                </itk:auditIdentity>\n"
			+ "                <itk:manifest count=\"1\">\n"
			+ "                    <itk:manifestitem base64=\"false\" compressed=\"false\"\n"
			+ "                        encrypted=\"false\"\n"
			+ "                        id=\"E808A967-49B2-498B-AD75-1D7A0F1262D7\"\n"
			+ "                        mimetype=\"text/xml\" profileid=\"urn:nhs-itk:services:201005:createPatient-v1-0\"/>\n"
			+ "                </itk:manifest>\n"
			+ "            </itk:header>\n"
			+ "            <itk:payloads count=\"1\">\n"
			+ "                <itk:payload id=\"E808A967-49B2-498B-AD75-1D7A0F1262D7\">\n"
			+ "                    <hl7v2:ADT_A05>\n"
			+ "                    </hl7v2:ADT_A05>\n"
			+ "                </itk:payload>\n"
			+ "            </itk:payloads>\n"
			+ "        </itk:DistributionEnvelope>\n"
			+ "    </soap:Body>\n"
			+ "</soap:Envelope>";


	@Test
	public void testParse() throws SAXException, IOException, ParserConfigurationException {
		Document doc = DomUtils.parse(TEST_XML);
		assertNotNull(doc);
		assertNotNull(doc.getDocumentElement());
		assertNotNull(doc.getFirstChild());
		String result = DomUtils.serialiseToXML(doc, DomUtils.PRETTY_PRINT);
		Logger.debug(result);
		
	}

}
