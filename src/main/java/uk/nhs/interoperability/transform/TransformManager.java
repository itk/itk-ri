/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transform;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class TransformManager.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class TransformManager {

	// This class would benefit from optimisation, but is kept simple for now
	// It will need some rationalisation w.r.t. template locations at least for RI
	
	/**
	 * Do transform.
	 *
	 * @param tname the tname
	 * @param input the input
	 * @return the string
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public static String doTransform(String tname, String input) throws ITKMessagingException {
		return doTransform(tname, input, null);
	}
	
	/**
	 * Do transform.
	 *
	 * @param tname the tname
	 * @param input the input
	 * @param parameters the parameters
	 * @return the string
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public static String doTransform(String tname, String input, Map<String, String> parameters) throws ITKMessagingException {
    	String output = "";
    	
	    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    InputStream tis = TransformManager.class.getResourceAsStream("/"+tname);
        StreamSource s = new StreamSource(tis);
        //StreamSource s = new StreamSource(f);
        Templates t = null;
        try {
			t = transformerFactory.newTemplates(s);
	        Transformer tf = t.newTransformer();
	        
	        //Set any stylesheet parameters as appropriate
	        if (parameters != null) {
				for (Map.Entry<String, String> entry: parameters.entrySet()) {;
					Logger.trace("Found a stylesheet parameter " + entry);
					tf.setParameter(entry.getKey(), entry.getValue());
				}
			}
	        
	        StreamSource s2 = new StreamSource(new StringReader(input));
	        StringWriter w = new StringWriter();
	        StreamResult r = new StreamResult(w);

	        tf.transform(s2, r);
	        output = w.getBuffer().toString();
	        //Logger.trace(output);
	        Logger.trace("Transformation complete");
		} catch (TransformerConfigurationException tce) {
			Logger.error("Error Building ITK Message",tce);
			throw new ITKMessagingException("Transformer Configuration Exception");
		} catch (TransformerException te) {
			Logger.error("Error Building ITK Message",te);
			throw new ITKMessagingException("Transformer Exception");
		}

        return output;

     }

}
