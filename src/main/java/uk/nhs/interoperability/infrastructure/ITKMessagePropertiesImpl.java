/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.infrastructure;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import uk.nhs.interoperability.transport.ITKTransportProperties;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;

/**
 * The Class ITKMessagePropertiesImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jone
 * @since 0.1
 */
public class ITKMessagePropertiesImpl implements ITKMessageProperties {
	

	/** The from address. */
	private ITKAddress fromAddress;
	
	/** The to address. */
	private ITKAddress toAddress;
	
	/** The audit identity. */
	private ITKIdentity auditIdentity ;
	
	/** The service id. */
	private String serviceId; 
	
	/** The business payload id. */
	private String businessPayloadId;
	
	/** The profile id. */
	private String profileId;
	
	/** The tracking id. */
	private String trackingId;
	
	/** The inbound transport properties. */
	private ITKTransportProperties inboundTransportProperties;
	
	//Create a map for handling specifications and make sure they are populated
	/** The handling specification. */
	private Map<String, String> handlingSpecification = new HashMap<String, String>();

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getTrackingId()
	 */
	public String getTrackingId() {
		return trackingId;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setTrackingId(java.lang.String)
	 */
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getFromAddress()
	 */
	public ITKAddress getFromAddress() {
		return fromAddress;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setFromAddress(uk.nhs.interoperability.infrastructure.ITKAddress)
	 */
	public void setFromAddress(ITKAddress fromAddress) {
		this.fromAddress = fromAddress;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getToAddress()
	 */
	public ITKAddress getToAddress() {
		return toAddress;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setToAddress(uk.nhs.interoperability.infrastructure.ITKAddress)
	 */
	public void setToAddress(ITKAddress toAddress) {
		this.toAddress = toAddress;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getAuditIdentity()
	 */
	public ITKIdentity getAuditIdentity() {
		return auditIdentity;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setAuditIdentity(java.lang.String)
	 */
	public void setAuditIdentity(ITKIdentity auditIdentity) {
		this.auditIdentity = auditIdentity;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getServiceId()
	 */
	public String getServiceId() {
		return serviceId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setServiceId(java.lang.String)
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getBusinessPayloadId()
	 */
	public String getBusinessPayloadId() {
		return businessPayloadId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setBusinessPayloadId(java.lang.String)
	 */
	public void setBusinessPayloadId(String businessPayloadId) {
		this.businessPayloadId = businessPayloadId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getProfileId()
	 */
	public String getProfileId() {
		return profileId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setProfileId(java.lang.String)
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getInboundTransportProperties()
	 */
	public ITKTransportProperties getInboundTransportProperties() {
		return inboundTransportProperties;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#setInboundTransportProperties(uk.nhs.interoperability.transport.ITKTransportProperties)
	 */
	public void setInboundTransportProperties(
			ITKTransportProperties inboundTransportProperties) {
		this.inboundTransportProperties = inboundTransportProperties;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getHandlingSpecifications()
	 */
	@Override
	public Map<String, String> getHandlingSpecifications() {
		return this.handlingSpecification;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#addHandlingSpecification(java.lang.String, java.lang.String)
	 */
	@Override
	public void addHandlingSpecification(String key, String value) {
		this.handlingSpecification.put(key, value);
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKMessageProperties#getHandlingSpecification(java.lang.String)
	 */
	@Override
	public String getHandlingSpecification(String key) {
		return this.handlingSpecification != null ? this.handlingSpecification.get(key) : null;
	}
	
	/**
	 * Extract distribution envelope from soap.
	 *
	 * @param doc the doc
	 * @return the document
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public static Document extractDistributionEnvelopeFromSoap(Document doc) throws ITKMessagingException {
		try {
			return DomUtils.createDocumentFromNode((Node)XPaths.SOAP_BODY_CONTENT_XPATH.evaluate(doc, XPathConstants.NODE));
		} catch (XPathExpressionException e) {
			throw new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not extract values from request", e);
		} catch (ParserConfigurationException e) {
			throw new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "XML parser configuration error", e);
		}
	}

	/**
	 * Builds the.
	 *
	 * @param distributionEnvelope the distribution envelope
	 * @return the iTK message properties
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public static ITKMessageProperties build(Document distributionEnvelope) throws ITKMessagingException {
		//Construct an empty itkMessageProperties
		ITKMessagePropertiesImpl itkMessageProperties = new ITKMessagePropertiesImpl();
		
		try {
			
			
			
			Double payloadCount = (Double)XPaths.ITK_PAYLOAD_COUNT_XPATH.evaluate(distributionEnvelope, XPathConstants.NUMBER);
			Logger.debug("Number of payloads " + payloadCount);
			
			//For now throw an exception if we encounter anything other than a single payload
			//TODO - add support for multiple payloads
			if (payloadCount != 1) {
				throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "The ITK reference implementation currently only supports a single payload within a distribution envelope");
			}
			
			String itkService = XPaths.ITK_SERVICE_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK service " + itkService);
			
			String itkAuditIdentityURI = XPaths.ITK_AUDIT_IDENTITY_URI_XPATH.evaluate(distributionEnvelope);
			String itkAuditIdentityType = XPaths.ITK_AUDIT_IDENTITY_TYPE_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK audit identity " + itkAuditIdentityType + ":" + itkAuditIdentityURI);
			
			String itkFromAddress = XPaths.ITK_FROM_ADDRESS_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK from address " + itkFromAddress);
			
			String itkToAddress = XPaths.ITK_FIRST_TO_ADDRESS_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK to address " + itkToAddress);
			
			String itkTrackingId = XPaths.ITK_TRACKING_ID_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK tracking id " + itkTrackingId);
			
			String itkProfileId = XPaths.ITK_PROFILE_ID_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK profile id " + itkProfileId);
			
			String businessPayloadId = XPaths.ITK_FIRST_PAYLOAD_ID_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK payload id " + businessPayloadId);
			
			String businessAckHandlingSpecification = XPaths.ITK_BUSINESS_ACK_HANDLING_SPECIFICATIONS_XPATH.evaluate(distributionEnvelope);
			Logger.debug("ITK businessAck handling specification " + businessAckHandlingSpecification);
			
			//DE = Payload properties (part of ITKMessageProperties API)
			itkMessageProperties.setBusinessPayloadId(businessPayloadId);
			itkMessageProperties.setToAddress(new ITKAddressImpl(itkToAddress));
			itkMessageProperties.setFromAddress(new ITKAddressImpl(itkFromAddress));
			itkMessageProperties.setServiceId(itkService);
			itkMessageProperties.setAuditIdentity(new ITKIdentityImpl(itkAuditIdentityURI, itkAuditIdentityType));
			itkMessageProperties.setProfileId(itkProfileId);
			itkMessageProperties.setTrackingId(itkTrackingId);
			itkMessageProperties.addHandlingSpecification(ITKMessageProperties.BUSINESS_ACK_HANDLING_SPECIFICATION_KEY, businessAckHandlingSpecification);
			//Also do we need to provide trackingId for any logging and correlation within Application?
			return itkMessageProperties;
			
		} catch (XPathExpressionException e) {
			throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not extract values from request", e);
		}
	}

}
