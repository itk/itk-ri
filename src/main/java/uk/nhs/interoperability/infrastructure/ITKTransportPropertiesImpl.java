/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.infrastructure;

import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

//import com.xmlsolutions.annotation.Requirement;

import uk.nhs.interoperability.transport.ITKTransportProperties;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.transport.ITKTransportRouteImpl;
import uk.nhs.interoperability.transport.WS.SOAPUtils;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.XPaths;

/**
 * The Class ITKTransportPropertiesImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKTransportPropertiesImpl implements ITKTransportProperties {
	
	/** The transport from. */
	private String transportFrom;
	
	/** The transport to. */
	private String transportTo;
	
	/** The invoked url. */
	private String invokedUrl;
	
	/** The transport relates to. */
	private String transportRelatesTo;
	
	/** The transport reply to. */
	private String transportReplyTo;
	
	/** The transport fault to. */
	private String transportFaultTo;
	
	/** The transport action. */
	private String transportAction;
	
	/** The transport message id. */
	private String transportMessageId;
	
	/** The transport type. */
	private String transportType;

	/**
	 * Instantiates a new iTK transport properties impl.
	 *
	 * @param transportType the transport type
	 */
	public ITKTransportPropertiesImpl(String transportType) {
		this.transportType = transportType;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportFrom()
	 */
	public String getTransportFrom() {
		return transportFrom;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportFrom(java.lang.String)
	 */
	public void setTransportFrom(String transportFrom) {
		this.transportFrom = transportFrom;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportTo()
	 */
	public String getTransportTo() {
		return transportTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportTo(java.lang.String)
	 */
	public void setTransportTo(String transportTo) {
		this.transportTo = transportTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getInvokedUrl()
	 */
	public String getInvokedUrl() {
		return invokedUrl;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setInvokedUrl(java.lang.String)
	 */
	public void setInvokedUrl(String invokedUrl) {
		this.invokedUrl = invokedUrl;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportRelatesTo()
	 */
	public String getTransportRelatesTo() {
		return transportRelatesTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportRelatesTo(java.lang.String)
	 */
	public void setTransportRelatesTo(String transportRelatesTo) {
		this.transportRelatesTo = transportRelatesTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportReplyTo()
	 */
	public String getTransportReplyTo() {
		return transportReplyTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportReplyTo(java.lang.String)
	 */
	public void setTransportReplyTo(String transportReplyTo) {
		this.transportReplyTo = transportReplyTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportFaultTo()
	 */
	public String getTransportFaultTo() {
		return transportFaultTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportFaultTo(java.lang.String)
	 */
	public void setTransportFaultTo(String transportFaultTo) {
		this.transportFaultTo = transportFaultTo;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportAction()
	 */
	public String getTransportAction() {
		return transportAction;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportAction(java.lang.String)
	 */
	public void setTransportAction(String transportAction) {
		this.transportAction = transportAction;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportMessageId()
	 */
	//@Requirement(traceTo="WS-ADR-01", status="implemented")
	public String getTransportMessageId() {
		return transportMessageId;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportMessageId(java.lang.String)
	 */
	//@Requirement(traceTo="WS-ADR-01", status="implemented")
	public void setTransportMessageId(String transportMessageId) {
		this.transportMessageId = transportMessageId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#setTransportType(java.lang.String)
	 */
	@Override
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportFaultToRoute()
	 */
	@Override
	public ITKTransportRoute getTransportFaultToRoute() {
		String address = SOAPUtils.resolveFaultToAddress(this);
		if (address != null) {
			return new ITKTransportRouteImpl(this.transportType, address);
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportProperties#getTransportReplyToRoute()
	 */
	@Override
	public ITKTransportRoute getTransportReplyToRoute() {
		String address = SOAPUtils.resolveReplyToAddress(this);
		Logger.trace("Reply to address " + address);
		if (address != null) {
			return new ITKTransportRouteImpl(this.transportType, address);
		}
		return null;
	}
	
	/**
	 * Builds the from soap.
	 *
	 * @param doc the doc
	 * @return the iTK transport properties
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	//@Requirement(traceTo="WS-EXT-03", status="not-started")
	public static ITKTransportProperties buildFromSoap(Document doc) throws ITKMessagingException {
		//Construct an empty itkTransportProperties
		ITKTransportProperties itkTransportProperties = new ITKTransportPropertiesImpl(ITKTransportRoute.HTTP_WS);
		
		try {
			//Extract some key properties from the SOAP envelope

			// WS_SO_001 : Validate that the To Element exists
			String transportTo = XPaths.WSA_TO_XPATH.evaluate(doc);
			if (transportTo.length()==0) {
				Logger.warn("SOAP To Element not populated - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.INVALID_MESSAGE_CODE, 
						"SOAP To Element not populated - message will be rejected" );
			}
			// WS_SO_002 : Validate that the To Element is valid
			// Implementation below is only for testing
			if (transportTo.toUpperCase().contains("INVALID")) {
				Logger.warn("SOAP To Element is invalid - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.INVALID_MESSAGE_CODE, 
						"SOAP To Element is invalid - message will be rejected" );
			}

			// WS_SO_003 : Validate that the Action Element exists
			String transportAction = XPaths.WSA_ACTION_XPATH.evaluate(doc);
			if (transportAction.length()==0) {
				Logger.warn("SOAP Action Element not populated - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.INVALID_MESSAGE_CODE, 
						"SOAP Action Element not populated - message will be rejected" );
			}

			String transportMessageId = XPaths.WSA_MSGID_XPATH.evaluate(doc);
			String transportReplyTo = XPaths.WSA_REPLY_TO_XPATH.evaluate(doc);
			String transportFaultTo = XPaths.WSA_FAULT_TO_XPATH.evaluate(doc);
			String transportFrom = XPaths.WSA_FROM_XPATH.evaluate(doc);
			
			// Security header fields
			// WS_SO_004 : Validate that the Timestamp Element exists
			String timestamp = XPaths.WSA_SECURITY_TIMESTAMP_XPATH.evaluate(doc);
			if (timestamp.length()==0){
				Logger.warn("SOAP Timestamp Element missing - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.INVALID_MESSAGE_CODE, 
						"SOAP Timestamp Element missing - message will be rejected" );
			}
			
			// WS_SO_006 : Validate that the Username Element exists
			String username  = XPaths.WSA_SECURITY_USERNAME_XPATH.evaluate(doc);
			if (username.length()==0){
				Logger.warn("SOAP Username Element missing - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.ACCESS_DENIED_CODE, 
						"SOAP Username Element missing - message will be rejected" );
			}

			// WS_SO_014 : Username must be valid for this service
			if (username.toUpperCase().contains("IINVALID")){
				Logger.warn("SOAP Username is invalid - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.ACCESS_DENIED_CODE, 
						"SOAP Username is invalid - message will be rejected" );
			}

			
			/*

			 * Set the WSA properties
			 * (not part of interface specification but useful internally within reference implementation)
			 */
			itkTransportProperties.setTransportMessageId(transportMessageId);
			itkTransportProperties.setTransportAction(transportAction);
			itkTransportProperties.setTransportFrom(transportFrom);
			itkTransportProperties.setTransportReplyTo(transportReplyTo);
			itkTransportProperties.setTransportFaultTo(transportFaultTo);
			itkTransportProperties.setTransportTo(transportTo);
			
			Logger.debug(itkTransportProperties.toString());
			
			return itkTransportProperties;
			
		} catch (XPathExpressionException e) {
			throw new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not extract values from request", e);
		} 
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[" + this.getClass().getCanonicalName() + "]"
					+ "\n\tMessage transport id " + this.getTransportMessageId()
					+ "\n\tMessage transport action " + this.getTransportAction()
					+ "\n\tMessage transport from " + this.getTransportFrom()
					+ "\n\tMessage transport to " + this.getTransportTo()
					+ "\n\tMessage transport replyTo " + this.getTransportReplyTo()
					+ "\n\tMessage transport faultTo " + this.getTransportFaultTo();
	}
}