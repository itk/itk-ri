/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport.WS;

import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;

/**
 * The Class ITKSOAPException.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKSOAPException extends ITKMessagingException {
	
	/**
	 * Instantiates a new iTKSOAP exception.
	 *
	 * @param itkMessagingException the itk messaging exception
	 */
	public ITKSOAPException(ITKMessagingException itkMessagingException) {
		//May throw NullPointer if itkMessagingException is null
		super(itkMessagingException.getRelatedMessageProperties(), itkMessagingException.getErrorCode(), itkMessagingException.getMessage(), itkMessagingException.getCause());
		super.setErrorId(itkMessagingException.getErrorId());
	}
	
	/**
	 * Instantiates a new iTKSOAP exception.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @param errorCode the error code
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public ITKSOAPException(ITKMessageProperties itkMessageProperties, int errorCode, String arg0, Throwable arg1) {
		super(itkMessageProperties, errorCode, arg0, arg1);
	}

	/**
	 * Instantiates a new iTKSOAP exception.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @param errorCode the error code
	 * @param arg0 the arg0
	 */
	public ITKSOAPException(ITKMessageProperties itkMessageProperties, int errorCode, String arg0) {
		super(itkMessageProperties, errorCode, arg0);
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4655001784827242123L;

	/**
	 * Instantiates a new iTKSOAP exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public ITKSOAPException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Instantiates a new iTKSOAP exception.
	 *
	 * @param arg0 the arg0
	 */
	public ITKSOAPException(String arg0) {
		super(arg0);
	}

	/**
	 * Instantiates a new iTKSOAP exception.
	 *
	 * @param arg0 the arg0
	 */
	public ITKSOAPException(Throwable arg0) {
		super(arg0);
	}
	
	/**
	 * Builds a complete soap wrapped SOAPFault.
	 *
	 * @return the string
	 */
	//@Requirement(traceTo={"WS-STD-03","WS-STD-04"})
	public String serialiseXML() {
		String XML = "<SOAPMessage>";
		XML += "<MessageId>" + UUID.randomUUID().toString().toUpperCase() + "</MessageId>";
		String faultActor = null;
		if (this.getRelatedItkTransportProperties() != null) {
			//Set props
			XML += "<To>" + SOAPUtils.resolveFaultToAddress(this.getRelatedItkTransportProperties()) + "</To>";
			XML += "<From>" + this.getRelatedItkTransportProperties().getTransportTo() + "</From>";
			XML += "<RelatesTo>"+ this.getRelatedItkTransportProperties().getTransportMessageId() + "</RelatesTo>";
			faultActor = this.getRelatedItkTransportProperties().getInvokedUrl(); 
		};
		XML += "<FaultDetail>";
		XML += "  <FaultLocation>Server</FaultLocation>";	
		if (faultActor != null) {
			XML += "  <FaultActor>" + faultActor + "</FaultActor>";
		}
		XML += "  <ErrorID>" + this.getErrorId() + "</ErrorID>";
		XML += "  <ErrorCode codeSystem=\"" + this.getErrorCodeSystem() + "\">" + this.getErrorCode() + "</ErrorCode>";
		XML += "  <ErrorText>" + this.decodeErrorCode() + "</ErrorText>";
		XML += "  <ErrorDiagnosticText>" + this.getLocalizedMessage() + "</ErrorDiagnosticText>";
		XML += "</FaultDetail>";
		XML += "</SOAPMessage>";
		String serialisedMessage ="";
		try {
			Logger.trace(XML);
			serialisedMessage = TransformManager.doTransform("ToSOAPFault.xslt", XML);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serialisedMessage;
	}
	
	
	/**
	 * Parses the soap fault.
	 *
	 * @param soapFaultXML the soap fault xml
	 * @return the iTKSOAP exception
	 */
	public static final ITKSOAPException parseSOAPFault(String soapFaultXML) {
		if (soapFaultXML != null) {
			try {
				Document doc = DomUtils.parse(soapFaultXML);
				Document faultDetail = DomUtils.createDocumentFromNode((Node)XPaths.WSA_SOAP_ERROR_DETAIL_XPATH.evaluate(doc, XPathConstants.NODE));
				Integer errorCode = Integer.parseInt(XPaths.WSA_SOAP_ERROR_DETAIL_CODE_XPATH.evaluate(faultDetail));
				String errorText = XPaths.WSA_SOAP_ERROR_DETAIL_DIAGNOSTIC_XPATH.evaluate(faultDetail);
				String errorId = XPaths.WSA_SOAP_ERROR_DETAIL_ID_XPATH.evaluate(faultDetail);
				ITKSOAPException soapFault = new ITKSOAPException(errorText);
				soapFault.setErrorId(errorId);
				soapFault.setErrorCode(errorCode);
				return soapFault;
			} catch (SAXException e) {
				Logger.error("Could not parse SOAP fault", e);
			} catch (IOException e) {
				Logger.error("Could not parse SOAP fault", e);
			} catch (ParserConfigurationException e) {
				Logger.error("Could not parse SOAP fault", e);
			} catch (XPathExpressionException e) {
				Logger.error("Could not parse SOAP fault", e);
			}
		}
		return new ITKSOAPException("SOAP fault (could not parse soap fault");
	}
	
	
	

}
