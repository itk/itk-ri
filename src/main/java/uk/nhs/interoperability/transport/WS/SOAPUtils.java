/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport.WS;

import uk.nhs.interoperability.transport.ITKTransportProperties;
import uk.nhs.interoperability.util.StringUtils;

/**
 * The Class SOAPUtils.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class SOAPUtils {

	/**
	 * Instantiates a new sOAP utils.
	 */
	private SOAPUtils() {
	}
	
	/**
	 * Resolve fault to address.
	 *
	 * @param itkTransportProperties the itk transport properties
	 * @return the string
	 */
	public static final String resolveFaultToAddress(ITKTransportProperties itkTransportProperties) {
		if (StringUtils.hasValue(itkTransportProperties.getTransportFaultTo())) {
			return itkTransportProperties.getTransportFaultTo();
		} else if (StringUtils.hasValue(itkTransportProperties.getTransportReplyTo())) {
			return itkTransportProperties.getTransportReplyTo();
		} 
		return null;
	}
	
	/**
	 * Resolve reply to address.
	 *
	 * @param itkTransportProperties the itk transport properties
	 * @return the string
	 */
	public static final String resolveReplyToAddress(ITKTransportProperties itkTransportProperties) {
		if (StringUtils.hasValue(itkTransportProperties.getTransportReplyTo())) {
			return itkTransportProperties.getTransportReplyTo();
		} 
		return null;
	}

}
