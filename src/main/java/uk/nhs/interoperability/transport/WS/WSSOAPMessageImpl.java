/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport.WS;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

//import com.xmlsolutions.annotation.Requirement;

import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.ITKMessageImpl;
import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.transport.ITKTransportRouteImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class WSSOAPMessageImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class WSSOAPMessageImpl extends ITKMessageImpl {

	/** The Constant SYNCREQ. */
	public static final String SYNCREQ = "SYNCREQ";
	
	/** The Constant SYNCRESP. */
	public static final String SYNCRESP = "SYNCRESP";
	
	/** The Constant ASYNCREQ. */
	public static final String ASYNCREQ = "ASYNCREQ";
	
	/** The Constant ASYNCRESP. */
	public static final String ASYNCRESP = "ASYNCRESP";
	
	/** The Constant DATE_FORMAT. */
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	
	/** The message id. */
	private String messageId;
	
	/** The action. */
	private String action;
	
	/** The from. */
	private String from;
	
	/** The to. */
	private String to;
	
	/** The username. */
	private String username;
	
	/** The created. */
	private Calendar created;
	
	/** The expires. */
	private Calendar expires;
	
	/** The payload. */
	private String payload;
	
	/** The ttl. */
	private int ttl;
	
	/** The msg type. */
	private String msgType;

	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Gets the from.
	 *
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Sets the from.
	 *
	 * @param from the new from
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Gets the to.
	 *
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Sets the to.
	 *
	 * @param to the new to
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the created.
	 *
	 * @return the created
	 */
	public Calendar getCreated() {
		return created;
	}
	
	/**
	 * Gets the created date string.
	 *
	 * @return the created date string
	 */
	public String getCreatedDateString() {
		return DATE_FORMAT.format(this.created.getTime());
	}

	/**
	 * Gets the expiry date string.
	 *
	 * @return the expiry date string
	 */
	public String getExpiryDateString() {
		if (null==expires){
			return "";
		} else {
			return DATE_FORMAT.format(this.expires.getTime());
		}
	}

	/**
	 * Gets the expires.
	 *
	 * @return the expires
	 */
	public Calendar getExpires() {
		return expires;
	}

	/**
	 * Gets the payload.
	 *
	 * @return the payload
	 */
	public String getPayload() {
		return payload;
	}
	
	/**
	 * Gets the msg type.
	 *
	 * @return the msg type
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * Sets the msg type.
	 *
	 * @param msgType the new msg type
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * Gets the reply to.
	 *
	 * @return the reply to
	 */
	public String getReplyTo() {
		return replyTo;
	}

	/**
	 * Sets the reply to.
	 *
	 * @param replyTo the new reply to
	 */
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	/**
	 * Gets the fault to.
	 *
	 * @return the fault to
	 */
	public String getFaultTo() {
		return faultTo;
	}

	/**
	 * Sets the fault to.
	 *
	 * @param faultTo the new fault to
	 */
	public void setFaultTo(String faultTo) {
		this.faultTo = faultTo;
	}

	/** The reply to. */
	String replyTo;
	
	/** The fault to. */
	String faultTo;
	
	/**
	 * Gets the time to live.
	 *
	 * @return the time to live
	 */
	public int getTimeToLive() {
		return ttl;
	}

	/**
	 * Sets the time to live.
	 *
	 * @param ttl the new time to live
	 */
	public void setTimeToLive(int ttl) {
		this.ttl = ttl;
		expires = (Calendar)created.clone();
		expires.add(Calendar.SECOND,ttl);
	}
	
	/**
	 * Instantiates a new wSSOAP message impl.
	 */
	public WSSOAPMessageImpl(){
		UUID soapMessageId = UUID.randomUUID();
		this.messageId = soapMessageId.toString().toUpperCase();
		this.created = Calendar.getInstance();
		this.username = "";
		this.action = "";
		this.faultTo = "";
		this.replyTo = "";
		this.from = "";
		this.payload = "";
		this.to = "";
	}
	
	public WSSOAPMessageImpl(ITKMessage itkMessage, String messageType) throws ITKMessagingException {
		this(null, itkMessage, messageType);
	}

	/**
	 * Instantiates a new wSSOAP message impl.
	 *
	 * @param destination the destination
	 * @param itkMessage the itk message
	 * @param messageType the message type
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public WSSOAPMessageImpl(ITKTransportRoute destination, ITKMessage itkMessage, String messageType) throws ITKMessagingException {
		//Initialise all variables
		this();
		if (itkMessage == null || messageType == null) {
			throw new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not add soap wrappers as message was null");
		}
		//Extract information from the wrapped message
		ITKMessageProperties itkMessageProperties = itkMessage.getMessageProperties();
		this.setMsgType(messageType);
		//Throw an error if the message properties are not present
		if (itkMessageProperties == null) {
			throw new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not add soap wrappers as the message did not contain the correct message properties");
		}

		// COR-DEH-01 : Set the action to the service
		this.setAction(itkMessageProperties.getServiceId());

		// Set the transport properties
		// Sync responses on the inbound channel won't have destination address
		if (destination!=null){
			this.setReplyTo(destination.getReplyToAddress());
			this.setFaultTo(destination.getExceptionToAddress());
			this.setTo(destination.getPhysicalAddress());
		}

		this.setPayload(itkMessage.getFullPayload());
		
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessageImpl#getPreresolvedRoute()
	 */
	@Override
	public ITKTransportRoute getPreresolvedRoute() {
		if (this.to != null) {
			return new ITKTransportRouteImpl(ITKTransportRoute.HTTP_WS, this.to);
		}
		return null;
	}
	
	/**
	 * Sets the payload.
	 *
	 * @param businessPayload the new payload
	 */
	public void setPayload(String businessPayload) {
		if (businessPayload != null) {
			if (businessPayload.startsWith("<?xml")) {
				//Remove any XML declarations
				this.businessPayload = businessPayload.substring(businessPayload.indexOf(">"));
			} else {
				this.businessPayload = businessPayload;
			}
		}
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getFullPayload()
	 */
	@Override
	public String getFullPayload() throws ITKMessagingException {
		return this.serialise();
	}

	//@Requirement(traceTo={"WS-STD-03","WS-STD-04","WS-SEC-04","WS-SEC-05"})
	/**
	 * Serialise.
	 *
	 * @return the string
	 */
	//@Requirement(traceTo="WS-STD-03", status="implemented")
	public String serialise(){
		String soapXML = "<SOAPMessage>";
		soapXML += "<MessageType>"+msgType+"</MessageType>";
		soapXML += "<MessageId>"+messageId+"</MessageId>";
		soapXML += "<Action>"+action+"</Action>";
		soapXML += "<From>"+from+"</From>";
		soapXML += "<To>"+to+"</To>";
		soapXML += "<FaultTo>"+faultTo+"</FaultTo>";
		soapXML += "<ReplyTo>"+replyTo+"</ReplyTo>";
		soapXML += "<Username>"+username+"</Username>";
		soapXML += "<Created>"+this.getCreatedDateString()+"</Created>";
		soapXML += "<Expires>"+this.getExpiryDateString()+"</Expires>";
		soapXML += "<Payload>" + this.businessPayload + "</Payload>";
		soapXML += "</SOAPMessage>";
		String SOAPPayload ="";
		try {
			SOAPPayload = TransformManager.doTransform("ToSOAP.xslt", soapXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Logger.trace(SOAPPayload);
		return SOAPPayload;
	}
}
