/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport.WS;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.infrastructure.ITKCommsException;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.infrastructure.ITKTransportTimeoutException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.transport.ITKSender;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.util.ITKApplicationProperties;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;

//import com.xmlsolutions.annotation.Requirement;

/**
 * The Class ITKSenderWSImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKSenderWSImpl implements ITKSender {

	/** The Constant WSSOAP_FROM. */
	private static final String WSSOAP_FROM = "wssoap.from";
	
	/** The Constant WSSECURITY_USERNAME. */
	private static final String WSSECURITY_USERNAME = "wssecurity.username";

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKSender#send(uk.nhs.interoperability.transport.ITKTransportRoute, uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public void send(ITKTransportRoute destination, ITKMessage request)
			throws ITKMessagingException {

		// Add the soap wrappers
		WSSOAPMessageImpl message = null;
		if (request.isResponse()) {
			message = new WSSOAPMessageImpl(destination, request, WSSOAPMessageImpl.ASYNCRESP);
		} else {
			message = new WSSOAPMessageImpl(destination, request, WSSOAPMessageImpl.SYNCREQ);
		}
		//WSSOAPMessageImpl message = new WSSOAPMessageImpl(destination, request, WSSOAPMessageImpl.SYNCREQ);
		message.setFrom(ITKApplicationProperties.getProperty(WSSOAP_FROM));
		message.setTo(destination.getPhysicalAddress());
		message.setUsername(ITKApplicationProperties.getProperty(WSSECURITY_USERNAME));
		message.setTimeToLive(destination.getTimeToLive());
		
		String SOAPPayload = message.getFullPayload();
			
		Document responseDoc = transportSend(destination, SOAPPayload, message.getAction());
		
		if (responseDoc==null){
		
			// No responseDoc means the call received a 202. Just pass back to the application.
			
		} else {
		
			// If a responseDoc is received, this is an error
			ITKMessagingException unknownResponseException = new ITKMessagingException(
						ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Unexpected Response");
			Logger.error("Unexpected Response",unknownResponseException);
			Logger.trace(DomUtils.serialiseToXML(responseDoc, DomUtils.PRETTY_PRINT));
			throw unknownResponseException;
		
		}

	
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKSender#sendSync(uk.nhs.interoperability.transport.ITKTransportRoute, uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	//@Requirement(traceTo={"WS-PAT-01"})
	public ITKMessage sendSync(ITKTransportRoute destination, ITKMessage request)
			throws ITKMessagingException {

		// Add the soap wrappers
		WSSOAPMessageImpl message = new WSSOAPMessageImpl(destination, request, WSSOAPMessageImpl.SYNCREQ);
		message.setFrom(ITKApplicationProperties.getProperty(WSSOAP_FROM));
		message.setTo(destination.getPhysicalAddress());
		message.setUsername(ITKApplicationProperties.getProperty(WSSECURITY_USERNAME));
		message.setTimeToLive(destination.getTimeToLive());
		//Obtain the on-the-wire message
		String SOAPPayload = message.getFullPayload();
		
		try {
			
			Document responseDoc = transportSend(destination, SOAPPayload, message.getAction());
			
			if (responseDoc == null) {
				// No responseDoc means the call received a 202 - this is an exception for Synchronous.
				ITKMessagingException unknownResponseException = new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "HTTP Acknowledgement only received (202).");
				Logger.error("Unknown response type",unknownResponseException);
				throw unknownResponseException;
				
			} else {
				
				// Pretty print the response message
				Logger.trace(DomUtils.serialiseToXML(responseDoc, DomUtils.PRETTY_PRINT));

				// Extract the SOAP Body Content
				Document businessPayloadDocument = DomUtils.createDocumentFromNode((Node)XPaths.SOAP_BODY_CONTENT_XPATH.evaluate(responseDoc, XPathConstants.NODE));

				if (businessPayloadDocument == null){
					// exception
					ITKMessagingException unknownResponseException = new ITKMessagingException(
							ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "No payload in SOAP Body");
					Logger.error("Unknown response type",unknownResponseException);
					throw unknownResponseException;
				}
				
				String responsePayloadString = DomUtils.serialiseToXML(businessPayloadDocument);
				
				//Construct the appropriate object for handing over to the application
				return new SimpleMessage(responsePayloadString);

			}
		} catch (ParserConfigurationException pce) {
			Logger.error("ParseConfigurationException on WS-CALL", pce);
			throw new ITKCommsException("XML Configuration Error Processing ITK Response");
		} catch (XPathExpressionException xpe) {
			Logger.error("XPathExpressionException reading payload on WS Response", xpe);
			throw new ITKCommsException("No Payload found in ITK Response");
		}
	
	}

	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKSender#sendAysnc(uk.nhs.interoperability.transport.ITKTransportRoute, uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	//@Requirement(traceTo={"WS-PAT-02"})
	public void sendAysnc(ITKTransportRoute destination, ITKMessage request)
			throws ITKMessagingException {
		
		// Add the soap wrappers
		WSSOAPMessageImpl message = null;
		
		// Message type is always ASYNCREQ here because
		// 1) True ASYNC responses (i.e. ADT Query Resp) should be sent using a response message on the Send API
		//    (response message is flagged by creating using inbound message properties)
		// 2) INF and BUS ACKS are actually new ASYNCREQ messages. Although they may be flagged as a response message
		//    (as they are created using inbound message properties) this is only for the purposes of routing.
		message = new WSSOAPMessageImpl(destination, request, WSSOAPMessageImpl.ASYNCREQ);

		message.setFrom(ITKApplicationProperties.getProperty(WSSOAP_FROM));
		message.setTo(destination.getPhysicalAddress());
		message.setUsername(ITKApplicationProperties.getProperty(WSSECURITY_USERNAME));
		message.setTimeToLive(destination.getTimeToLive());
		//Obtain the on-the-wire message
		String SOAPPayload = message.getFullPayload();
		
		try {
			
			Document responseDoc = transportSend(destination, SOAPPayload, message.getAction());
			
			if (responseDoc == null) {
				// No responseDoc means the call received a HTTP 202. OK for ASYNC				
			} else {
				
				//Obtain the actual business payload - should be SMR or else this is an error.
				Document businessPayload = DomUtils.createDocumentFromNode((Node)XPaths.SOAP_WRAPPED_ITK_SIMPLE_MESSAGE_RESPONSE_XPATH.evaluate(responseDoc, XPathConstants.NODE));
				
				// If no known payload then throw an exception
				if (businessPayload==null){
					ITKMessagingException unknownResponseException = new ITKMessagingException("Unknown Response Type");
					Logger.error("Unknown response type",unknownResponseException);
					Logger.trace(DomUtils.serialiseToXML(businessPayload, DomUtils.PRETTY_PRINT));
					throw unknownResponseException;
				}
				
			}
			
		} catch (ParserConfigurationException pce) {
			Logger.error("ParseConfigurationException on WS-CALL", pce);
			throw new ITKCommsException("XML Configuration Error Processing ITK Response");
		} catch (XPathExpressionException xpe) {
			Logger.error("XPathExpressionException reading payload on WS Response", xpe);
			throw new ITKCommsException("No Payload found in ITK Response");
		}
	
	}
	
	/**
	 * Transport send.
	 *
	 * @param destination the destination
	 * @param SOAPPayload the sOAP payload
	 * @return the document
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	//@Requirement(traceTo={"WS-PAT-01","WS-PAT-02"})
	private Document transportSend(ITKTransportRoute destination, String SOAPPayload, String SOAPAction)
			throws ITKMessagingException {
		
		Document responseDoc = null;
		// Post the msg
		String serviceEndpoint = destination.getPhysicalAddress();
		try {
			URLConnection urlConnection = 
					new URL(serviceEndpoint).openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			conn.setUseCaches(false);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type","text/xml");
			conn.setRequestProperty("accept-charset","UTF-8");
			conn.setRequestProperty("SOAPAction",SOAPAction);
			conn.setConnectTimeout(30000); 
			PrintWriter pw = new PrintWriter(conn.getOutputStream());
			pw.write(SOAPPayload);
			pw.close();
			int responseCode = conn.getResponseCode();
			Logger.trace("HTTP Response Code:"+responseCode);
			if (responseCode == HttpServletResponse.SC_ACCEPTED){
				
				Logger.trace("SIMPLE HTTP ACCEPT (202)");
				
			} else if (responseCode == HttpServletResponse.SC_OK) {
				Logger.trace("HTTP 200");
				String responseString = readInput(conn.getInputStream());
				responseDoc = DomUtils.parse(responseString);
				
			} else if (responseCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				Logger.trace("HTTP 500");
				String responseString = readInput(conn.getErrorStream());
				//Understand why an error has occurred - do we have a SOAP fault?				
				if (responseString != null && responseString.contains("http://www.w3.org/2005/08/addressing/fault")) {
					//SOAP fault
					throw ITKSOAPException.parseSOAPFault(responseString);
				} else {
					throw new ITKCommsException("HTTP Internal server error");
				}
			} else {
				
				Logger.trace("Unrecognized HTTP response code:"+responseCode);
				throw new ITKCommsException("Unrecognized HTTP response code:"+responseCode);

			}
			
		} catch (MalformedURLException mue) {
			Logger.error("MalformedURLException on WS-CALL", mue);
			throw new ITKCommsException("Configuration error sending ITK Message");
		} catch (SocketTimeoutException ste) {
			Logger.error("Timeout on WS-CALL", ste);
			throw new ITKTransportTimeoutException("Transport timeout sending ITK Message");
		} catch (IOException ioe) {
			Logger.error("IOException on WS-CALL", ioe);
			throw new ITKCommsException("Transport error sending ITK Message");
		} catch (SAXException se) {
			Logger.error("SAXException processing response from WS-CALL", se);
			throw new ITKCommsException("XML Error Processing ITK Response");
		} catch (ParserConfigurationException pce) {
			Logger.error("ParseConfigurationException on WS-CALL", pce);
			throw new ITKCommsException("XML Configuration Error Processing ITK Response");
		}
		
		return responseDoc;
		
	}
	
	/**
	 * Read input.
	 *
	 * @param is the is
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String readInput(InputStream is) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(is);
		byte[] contents = new byte[1024];
		int bytesRead = 0;
		String responseString = "";
		while ((bytesRead = bis.read(contents)) != -1) {
			responseString = responseString + new String(contents, 0, bytesRead);
		}
		Logger.trace("Response was:"+responseString);
		bis.close();
		
		return responseString ;
	}
}
