/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.nhs.interoperability.infrastructure.ITKAckDetails;
import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessageImpl;
import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class ITKInfrastructureAck.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKInfrastructureAck extends ITKMessageImpl implements ITKAckDetails {
	
	/** The Constant ACK_PROFILE_ID. */
	public static final String ACK_PROFILE_ID = "urn:nhs-en:profile:ITKInfrastructureAcknowledgement-v1-0";
	
	/** The Constant ACK_SERVICE_ID. */
	public static final String ACK_SERVICE_ID = "urn:nhs-itk:ns:201005:InfrastructureAcknowledgment";
	
	/** The Constant DATE_FORMAT. */
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	
	/** The ref to tracking id. */
	private String refToTrackingId;
	
	/** The ref to service id. */
	private String refToServiceId;
	
	/** The audit identity. */
	private String auditIdentity;
	
	/** The itk messaging exception. */
	private ITKMessagingException itkMessagingException;
	
	/**
	 * Instantiates a new iTK infrastructure ack.
	 *
	 * @param requestMessageProperties the request message properties
	 * @param auditIdentity the audit identity
	 */
	public ITKInfrastructureAck(ITKMessageProperties requestMessageProperties, String auditIdentity) {
		this(requestMessageProperties, auditIdentity, null);
	}
	
	/**
	 * Instantiates a new iTK infrastructure ack.
	 *
	 * @param requestMessageProperties the request message properties
	 * @param auditIdentity the audit identity
	 * @param itkMessagingException the itk messaging exception
	 */
	public ITKInfrastructureAck(ITKMessageProperties requestMessageProperties, String auditIdentity, ITKMessagingException itkMessagingException) {
		super(requestMessageProperties, auditIdentity, ACK_PROFILE_ID , true);
		this.refToTrackingId = requestMessageProperties.getTrackingId();
		this.refToServiceId = requestMessageProperties.getServiceId();
		this.auditIdentity = auditIdentity;
		this.itkMessagingException = itkMessagingException;
		/*
		 * Service is not simply request service with "Response" suffix.
		 * InfrastructureAck has its own specific service
		 */
		super.getMessageProperties().setServiceId(ACK_SERVICE_ID);
		this.setIsReponse(true);
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getFullPayload()
	 */
	@Override
	public String getFullPayload() throws ITKMessagingException {
		String fullPayload = "";
		
		String XML = "<ITKInfraAck>";		
		XML += "<RefToTrackingId>" + this.refToTrackingId + "</RefToTrackingId>";
		XML += "<RefToService>"+this.refToServiceId+"</RefToService>";
		XML += "<AuditId>"+this.auditIdentity+"</AuditId>";
		XML += "<Created>" + DATE_FORMAT.format(Calendar.getInstance().getTime()) + "</Created>";
		
		
		if (this.itkMessagingException != null) {
			ITKMessagingException ex = this.itkMessagingException;
			XML += "<FaultDetail>";
			XML += "  <ErrorID>" + ex.getErrorId() + "</ErrorID>";
			XML += "  <ErrorCode codeSystem=\"" + ex.getErrorCodeSystem() + "\">" + ex.getErrorCode() + "</ErrorCode>";
			XML += "  <ErrorText>" + ex.decodeErrorCode() + "</ErrorText>";
			XML += "  <ErrorDiagnosticText>" + ex.getLocalizedMessage() + "</ErrorDiagnosticText>";
			XML += "</FaultDetail>";
		}
		XML += "</ITKInfraAck>";

		fullPayload = TransformManager.doTransform("ToInfrastructureAck.xslt", XML);

		return fullPayload;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessageImpl#getBusinessPayload()
	 */
	@Override
	public String getBusinessPayload() {
		try {
			return this.getFullPayload();
		} catch (ITKMessagingException e) {
			Logger.error("Could not create infrastructure ack payload", e);
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKAckDetails#getIntendedRecipient()
	 */
	@Override
	public ITKAddress getIntendedRecipient() {
		return this.messageProperties != null ? this.messageProperties.getFromAddress() : null;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKAckDetails#getNackError()
	 */
	@Override
	public ITKMessagingException getNackError() {
		return this.itkMessagingException;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKAckDetails#getReportingIdentity()
	 */
	@Override
	public String getReportingIdentity() {
		return this.auditIdentity;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKAckDetails#getServiceRef()
	 */
	@Override
	public String getServiceRef() {
		return this.refToServiceId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKAckDetails#getTrackingRef()
	 */
	@Override
	public String getTrackingRef() {
		return this.refToTrackingId;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.infrastructure.ITKAckDetails#isNack()
	 */
	@Override
	public boolean isNack() {
		return this.getNackError() != null;
	}

}
