/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport;

/**
 * The Class ITKTransportRouteImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKTransportRouteImpl implements ITKTransportRoute {

	/** The transport type. */
	private String transportType;
	
	/** The physical address. */
	private String physicalAddress;
	
	/** The reply to address. */
	private String replyToAddress;
	
	/** The exception to address. */
	private String exceptionToAddress;
	
	/** The destination type. */
	private String destinationType;
	
	/** The wrapper type. */
	private String wrapperType = ITKTransportRoute.NO_WRAPPER;
	
	/** The time to live. */
	private int timeToLive;
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getTransportType()
	 */
	@Override
	public String getTransportType() {
		return transportType;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getPhysicalAddress()
	 */
	@Override
	public String getPhysicalAddress() {
		return physicalAddress;
	}
	
	/**
	 * Instantiates a new iTK transport route impl.
	 *
	 * @param type the type
	 * @param address the address
	 */
	public ITKTransportRouteImpl(String type, String address) {
		this.transportType = type;
		this.physicalAddress = address;
	}
	
	/**
	 * Instantiates a new iTK transport route impl.
	 *
	 * @param type the type
	 * @param address the address
	 * @param replyTo the reply to
	 * @param exceptionTo the exception to
	 * @param destinationType the destination type
	 * @param wrapperType the wrapper type
	 * @param timeToLive the time to live
	 */
	public ITKTransportRouteImpl(String type, String address, String replyTo, 
								 String exceptionTo, String destinationType, 
								 String wrapperType, int timeToLive){
		this.transportType = type;
		this.physicalAddress = address;
		this.replyToAddress = replyTo;
		this.exceptionToAddress = exceptionTo;
		this.destinationType = destinationType;
		this.wrapperType = wrapperType;
		this.timeToLive = timeToLive;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getReplyToAddress()
	 */
	@Override
	public String getReplyToAddress() {
		return replyToAddress;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getExceptionToAddress()
	 */
	@Override
	public String getExceptionToAddress() {
		return exceptionToAddress;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getDestinationType()
	 */
	@Override
	public String getDestinationType() {
		return destinationType;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getWrapperType()
	 */
	@Override
	public String getWrapperType() {
		return wrapperType;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#setWrapperType(java.lang.String)
	 */
	@Override
	public void setWrapperType(String wrapperType) {
		this.wrapperType = wrapperType;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.transport.ITKTransportRoute#getTimeToLive()
	 */
	@Override
	public int getTimeToLive() {
		return timeToLive;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[" + this.getClass().getCanonicalName() + "]"
					+ "\n\tTransport type " + this.getTransportType()
					+ "\n\tPhysical address " + this.getPhysicalAddress()
					+ "\n\tWrapper type " + this.getWrapperType();
	}
	

}
