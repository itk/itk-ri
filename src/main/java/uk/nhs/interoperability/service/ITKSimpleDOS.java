/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.service;

import uk.nhs.interoperability.capabilities.DirectoryOfServices;
import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKServiceImpl;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.transport.ITKTransportRouteImpl;
import uk.nhs.interoperability.util.ITKDirectoryProperties;
import uk.nhs.interoperability.util.ITKServiceProperties;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class ITKSimpleDOS.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKSimpleDOS implements DirectoryOfServices {

	/** The Constant IS_SUPPORTED. */
	private static final String IS_SUPPORTED = "supported";
	
	/** The Constant SUPPORTS_ASYNC. */
	private static final String SUPPORTS_ASYNC = "supportsAsync";
	
	/** The Constant SUPPORTS_SYNC. */
	private static final String SUPPORTS_SYNC = "supportsSync";
	
	/** The Constant IS_BASE64. */
	private static final String IS_BASE64 = "isBase64";
	
	/** The Constant MIME_TYPE. */
	private static final String MIME_TYPE = "mimeType";

	/** The Constant CHANNELID. */
	private static final String CHANNELID = "channelid";
	
	/** The Constant TIME_TO_LIVE. */
	private static final String TIME_TO_LIVE = "TimeToLive";
	
	/** The Constant WRAPPER_TYPE. */
	private static final String WRAPPER_TYPE = "WrapperType";
	
	/** The Constant DESTINATION_TYPE. */
	private static final String DESTINATION_TYPE = "DestinationType";
	
	/** The Constant EXCEPTION_TO. */
	private static final String EXCEPTION_TO = "ExceptionTo";
	
	/** The Constant DEFAULT. */
	private static final String DEFAULT = "DEFAULT";
	
	/** The Constant REPLY_TO. */
	private static final String REPLY_TO = "ReplyTo";
	
	/** The Constant ROUTE_TYPE. */
	private static final String ROUTE_TYPE = "RouteType";
	
	/** The Constant PHYSICAL_DESTINATION. */
	private static final String PHYSICAL_DESTINATION = "PhysicalDestination";

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.capabilities.DirectoryOfServices#resolveDestination(java.lang.String, uk.nhs.interoperability.infrastructure.ITKAddress)
	 */
	@Override
	public ITKTransportRoute resolveDestination(String serviceId, ITKAddress address) {
		
		ITKTransportRoute route = null;
		
		String svc = serviceId;
		String add = address.getURI();
		String channelKey = svc + "." + add + "." + CHANNELID;
		Logger.trace("Channel Key:"+channelKey);
		
		String channelId = ITKDirectoryProperties.getProperty(channelKey);
		
		if (null!=channelId){
			Logger.trace("Channel Id:"+channelId);
			String physicalDestination = getDirectoryProperty(channelId,PHYSICAL_DESTINATION);
			String routeType = getDirectoryProperty(channelId,ROUTE_TYPE);
			String replyTo = getDirectoryProperty(channelId,REPLY_TO);
			String exceptionTo = getDirectoryProperty(channelId,EXCEPTION_TO);
			String destinationType = getDirectoryProperty(channelId,DESTINATION_TYPE);
			String wrapperType = getDirectoryProperty(channelId,WRAPPER_TYPE);
			String timeToLive = getDirectoryProperty(channelId,TIME_TO_LIVE);

			// time to live
			int ttl = 30*60;
			if (null!=timeToLive){
				ttl = Integer.parseInt(timeToLive);
			}
			route = new ITKTransportRouteImpl(routeType,physicalDestination,
											  replyTo,exceptionTo,destinationType,wrapperType,ttl);
		}
		
		return route;
	}

	/**
	 * Gets the directory property.
	 *
	 * @param channelId the channel id
	 * @param propertyName the property name
	 * @return the directory property
	 */
	private String getDirectoryProperty(String channelId, String propertyName){
		String propertyValue = ITKDirectoryProperties.getProperty(channelId+"."+propertyName);
		if (propertyValue==null){
			propertyValue = ITKDirectoryProperties.getProperty(DEFAULT+"."+propertyName);
		}
		if (propertyValue==null){
			propertyValue = "";
		}
		return propertyValue;
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.capabilities.DirectoryOfServices#getService(java.lang.String)
	 */
	public ITKService getService(String serviceId) {
		
		ITKServiceImpl service = new ITKServiceImpl(serviceId);

		boolean isSupported = getServiceBooleanProperty(serviceId, IS_SUPPORTED);
		if (!isSupported) {
			return null;
		}
		service.setBase64(getServiceBooleanProperty(serviceId, IS_BASE64));
		service.setSupportsSync(getServiceBooleanProperty(serviceId, SUPPORTS_SYNC));
		service.setSupportsAsync(getServiceBooleanProperty(serviceId, SUPPORTS_ASYNC));
		service.setMimeType(getServiceProperty(serviceId, MIME_TYPE));

		return service;
	}
	
	/**
	 * Gets the service property.
	 *
	 * @param serviceId the service id
	 * @param propertyName the property name
	 * @return the service property
	 */
	private String getServiceProperty(String serviceId, String propertyName){
	
		String propertyValue = ITKServiceProperties.getProperty(serviceId+"."+propertyName);
		if (propertyValue==null){
			propertyValue = ITKServiceProperties.getProperty(DEFAULT+"."+propertyName);
		}
		if (propertyValue==null){
			propertyValue = "";
		}
		return propertyValue;
	}
	
	/**
	 * Gets the service boolean property.
	 *
	 * @param serviceId the service id
	 * @param propertyName the property name
	 * @return the service boolean property
	 */
	private boolean getServiceBooleanProperty(String serviceId, String propertyName){
		
		boolean propertyValue = false;
		if (!propertyName.equals("")){

			String serviceProperty = ITKServiceProperties.getProperty(serviceId+"."+propertyName);
			
			if (null!=serviceProperty){
				if (serviceProperty.equals("Y")){
					propertyValue = true;
				}
			} else {
				serviceProperty = ITKServiceProperties.getProperty("DEFAULT."+propertyName);
				
			}
		}
		return propertyValue;
	}
}
