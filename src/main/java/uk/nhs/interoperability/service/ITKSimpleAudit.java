/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import uk.nhs.interoperability.capabilities.AuditException;
import uk.nhs.interoperability.capabilities.AuditService;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;

//import com.xmlsolutions.annotation.Requirement;

/**
 * The Class ITKSimpleAudit.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
//@Requirement(traceTo={"IFA-SEC-02"}, status="in-progress")
public class ITKSimpleAudit implements AuditService {
	
	/** The Constant DATE_FORMAT. */
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	/** The Constant CAL. */
	private static final Calendar CAL = Calendar.getInstance();
	
	/** The Constant _INSTANCE. */
	private static final ITKSimpleAudit _INSTANCE = new ITKSimpleAudit();

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.capabilities.AuditService#auditEvent(java.lang.String, long, uk.nhs.interoperability.infrastructure.ITKMessageProperties)
	 */
	@Override
	//@Requirement(traceTo={"COR-DEH-04"}, status="in-progress")
	public void auditEvent(String event, long eventTime, ITKMessageProperties itkMessageProperties) throws AuditException {
		if (itkMessageProperties == null && event == null) {
			throw new AuditException("Could not write audit as either the ITKMessageProperties and/or event was not provided");
		}
		System.out.println(DATE_FORMAT.format(CAL.getTime()) 
					+ " [AUDIT] " + event + " at " + DATE_FORMAT.format(new Date(eventTime))
					+ " from " + itkMessageProperties.getAuditIdentity());
		
	}
	
	/**
	 * Gets the single instance of ITKSimpleAudit.
	 *
	 * @return single instance of ITKSimpleAudit
	 */
	public static final AuditService getInstance() {
		return _INSTANCE;
	}

}
