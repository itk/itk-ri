/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.source;

import java.io.IOException;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.capabilities.DirectoryOfServices;
import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKCommsException;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.DEWrappedMessage;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.service.ITKService;
import uk.nhs.interoperability.service.ITKSimpleDOS;
import uk.nhs.interoperability.transport.ITKSender;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.transport.WS.ITKSenderWSImpl;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;

/**
 * The Class ITKMessageSenderImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKMessageSenderImpl implements ITKMessageSender {
	
	/** The dos. */
	private DirectoryOfServices directoryOfServices;
	
	/**
	 * Instantiates a new iTK message sender impl.
	 */
	public ITKMessageSenderImpl() {
		this.directoryOfServices = new ITKSimpleDOS();
	}

	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.source.ITKMessageSender#send(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public void send(ITKMessage request) throws ITKMessagingException {

		// Get the ITKService from the DirectoryOfService
		String serviceId = request.getMessageProperties().getServiceId();
		ITKService service = directoryOfServices.getService(serviceId);
		if (service==null){
			Logger.trace("Invalid Service");
			throw new ITKMessagingException("Service " + serviceId + " is not a configured service (see service.properties).");	
		}

		ITKTransportRoute route = getRoute(request);

		ITKSender sender = getSender(route);

		ITKMessage message = buildMessage(request, route, service);

		Logger.trace("Sending via WS: " + message.getFullPayload());

		sender.send(route, message);
		
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.source.ITKMessageSender#sendSync(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public ITKMessage sendSync(ITKMessage request) throws ITKMessagingException {

		// Get the ITKService from the DirectoryOfService
		String serviceId = request.getMessageProperties().getServiceId();
		ITKService service = directoryOfServices.getService(serviceId);
		if (service==null){
			Logger.trace("Invalid Service");
			throw new ITKMessagingException("Service " + serviceId + " is not a configured service (see service.properties).");	
		}

		if (!service.supportsSync()){
			Logger.trace("Invalid Service Call");
			throw new ITKMessagingException("Service " + service.getServiceId() + " can not be called Synchronously");
		}

		ITKTransportRoute route = getRoute(request);

		ITKSender sender = getSender(route);

		ITKMessage message = buildMessage(request, route, service);

		Logger.trace("Sending via configured transport: " + message.getFullPayload());
		ITKMessage response = sender.sendSync(route, message);
		
		ITKMessage simpleResponse = buildResponse(response);
		
		return simpleResponse;

	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.source.ITKMessageSender#sendAsync(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public void sendAsync(ITKMessage request) throws ITKMessagingException {

		// Get the ITKService from the DirectoryOfService
		String serviceId = request.getMessageProperties().getServiceId();
		ITKService service = directoryOfServices.getService(serviceId);
		if (service==null){
			Logger.trace("Invalid Service");
			throw new ITKMessagingException("Service " + serviceId + " is not a configured service (see service.properties).");	
		}

		if (!service.supportsAsync()){
			Logger.trace("Invalid Service Call");
			throw new ITKMessagingException("Service " + service.getServiceId() + " can not be called Asynchronously");
		}

		ITKTransportRoute route = getRoute(request);

		ITKSender sender = getSender(route);

		ITKMessage message = buildMessage(request, route, service);

		Logger.trace("Sending via configured transport: " + message.getFullPayload());
		sender.sendAysnc(route, message);

	}

	/**
	 * Gets the route.
	 *
	 * @param request the request
	 * @return the route
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKTransportRoute getRoute(ITKMessage message) throws ITKMessagingException {

		String serviceId = message.getMessageProperties().getServiceId();

		//Has the transport route already been resolved? This may be set on a response
		ITKTransportRoute route = message.getPreresolvedRoute();
		ITKAddress toAddress = message.getMessageProperties().getToAddress();
		
		
		//If the route has not been pre-resolved look it up
		
		if (route == null) {
			
		    Logger.trace("TransportRoute has not been pre-resolved - looking up");
		    			
			// Resolve Destination Service in terms of a TransportRoute
			route = directoryOfServices.resolveDestination(serviceId, toAddress);
			
		} else {
			
			Logger.debug("Pre-resolved transport route on object " + message.getClass().getSimpleName() +  " is " + route);

		}
		
		if (route == null){
			String msgStr = "No route found for service (" + serviceId + ") and recipient ("+ toAddress + ")";
			Logger.trace(msgStr);
			throw new ITKMessagingException(msgStr);
		} else {
			
			Logger.debug("Found transport route:" + route);
			
		}
		
		
		return route;
	}
	
	/**
	 * Builds the message.
	 *
	 * @param request the request
	 * @param route the route
	 * @param service the service
	 * @return the iTK message
	 */
	private ITKMessage buildMessage(ITKMessage request, ITKTransportRoute route, ITKService service){
		ITKMessage message = null;

		if (route.getWrapperType().equals(ITKTransportRoute.DISTRIBUTION_ENVELOPE)){
			Logger.trace("Adding distribution envelope wrapper");
			
			if (service.isBase64()) {
				String b64DocText = DatatypeConverter.printBase64Binary(request.getBusinessPayload().getBytes());
				request.setBusinessPayload(b64DocText);
			}
			message = new DEWrappedMessage(service, request, DEWrappedMessage.DE_ADDRESSED); 
		} else {
			Logger.trace("No DE wrapper required");
			message = request;
		}
		return message;		
	}
	
	/**
	 * Gets the sender.
	 *
	 * @param route the route
	 * @return the sender
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKSender getSender(ITKTransportRoute route) throws ITKMessagingException {
		
		ITKSender sender = null;
		// Resolve the sender implementation according to the transport type
		if (route.getTransportType().equals(ITKTransportRoute.HTTP_WS)){
			sender = new ITKSenderWSImpl();
		}

		if (sender == null){
			Logger.trace("No Transport Sender Found");
			throw new ITKMessagingException("No transport implementation found for configured route");
		}

		return sender;
	}


	/**
	 * Builds the response.
	 *
	 * @param response the response
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage buildResponse(ITKMessage response) throws ITKMessagingException {
		
		ITKMessage simpleResponse = new SimpleMessage();
		ITKMessageProperties itkMessageProperties = null;
		
		try {
			
			String responsePayloadString = "";
			Document responseDoc = DomUtils.parse(response.getBusinessPayload());
			String responsePayloadName = responseDoc.getDocumentElement().getLocalName();
			Logger.trace("Received:"+responsePayloadName);
			
			if (responsePayloadName.equalsIgnoreCase("DistributionEnvelope")){
				Logger.trace("Processing DistributionEnvelope");
	
				//Extract message properties from the response
				itkMessageProperties = ITKMessagePropertiesImpl.build(responseDoc);
	
				// Check plain payload
				String mimetype = (String)XPaths.ITK_FIRST_MIMETYPE_XPATH.evaluate(responseDoc);
				Logger.debug("MimeType: " + mimetype);
				
				if (mimetype.equalsIgnoreCase("text/plain")){
					// Get the payload as a text node
					String payload = (String)XPaths.ITK_FIRST_PAYLOAD_TEXT_XPATH.evaluate(responseDoc);
					responsePayloadString = payload;
					
					// Check base64 encoding
					String base64 = (String)XPaths.ITK_FIRST_BASE64_XPATH.evaluate(responseDoc);
					Logger.debug("Base64: " + base64);
					if (base64.equalsIgnoreCase("true")){
						byte[] payloadBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(payload);
						responsePayloadString = new String(payloadBytes);
					}
					
				} else {
					// Get the first payload from the distribution envelope
					Document businessPayloadDocument = DomUtils.createDocumentFromNode((Node)XPaths.ITK_FIRST_PAYLOAD_XPATH.evaluate(responseDoc, XPathConstants.NODE));
					
					if (businessPayloadDocument == null){
						// exception
						ITKMessagingException unknownResponseException = new ITKMessagingException(
								ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "No payload in Distribution Envelope");
						Logger.error("Unknown response type",unknownResponseException);
						throw unknownResponseException;
					}
					
					//Show the extracted payload in the trace log
					String businessPayloadName = businessPayloadDocument.getDocumentElement().getLocalName();
					Logger.trace("Business Payload Name is:"+businessPayloadName);
					responsePayloadString = DomUtils.serialiseToXML(businessPayloadDocument);
				}
				
			} else if (responsePayloadName.equalsIgnoreCase("SimpleMessageResponse")){
				Logger.trace("Processing SimpleMessageResponse");
				// Set up a dummy MessageProperties for the audit process. There is no audit identifier on a Simple Message Response
				itkMessageProperties = new ITKMessagePropertiesImpl();
				itkMessageProperties.setAuditIdentity(new ITKIdentityImpl("NOT SPECIFIED"));
				responsePayloadString = (String)XPaths.ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT_XPATH.evaluate(responseDoc);
				
			} else {
				Logger.trace("Processing UNKNOWN");
				ITKMessagingException unknownResponseException = new ITKMessagingException(
						ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Neither DistributionEnvelope nor SimpleMessageResponse Found");
				Logger.error("Unknown response type",unknownResponseException);
				throw unknownResponseException;
				
			}
			
			simpleResponse.setBusinessPayload(responsePayloadString);
			simpleResponse.setMessageProperties(itkMessageProperties);
			
		} catch (SAXException se) {
			Logger.error("SAXException processing response from Transport", se);
			throw new ITKCommsException("XML Error Processing ITK Response");
		} catch (IOException ioe) {
			Logger.error("IOException on Transport", ioe);
			throw new ITKCommsException("Transport error sending ITK Message");
		} catch (ParserConfigurationException pce) {
			Logger.error("ParseConfigurationException on Transport", pce);
			throw new ITKCommsException("XML Configuration Error Processing ITK Response");
		} catch (XPathExpressionException xpe) {
			Logger.error("XPathExpressionException reading payload on Transport Response", xpe);
			throw new ITKCommsException("No Payload found in ITK Response");
		}
		
		return simpleResponse;
	}

}
