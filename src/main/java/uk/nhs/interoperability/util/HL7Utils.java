/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * The Class HL7Utils.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class HL7Utils {
	
	/** The Constant HL7_DATE_FORMAT. */
	private static final SimpleDateFormat HL7_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmm");

	/**
	 * Instantiates a new h l7 utils.
	 */
	private HL7Utils() {
		//prevent direct instantiation
	}
	
	/**
	 * Create a new HL7DateTime string of the form
	 * <code>201107121534</code> e.g. <code>yyyyMMddHHmm</code> format
	 * @return A new HL7 date time string based on the instant the method was called
	 */
	public static final String getHL7DateTime() {
		return HL7_DATE_FORMAT.format(Calendar.getInstance().getTime());
	}

}
