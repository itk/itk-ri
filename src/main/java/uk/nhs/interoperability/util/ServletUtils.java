/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

/**
 * The Class ServletUtils.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ServletUtils {
	
	//TODO make this more efficient (don't read one byte at a time)
	/**
	 * Read request content.
	 *
	 * @param req the req
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final byte[] readRequestContent(HttpServletRequest req) throws IOException {
			if (req != null) {
				Logger.debug("Reading servlet request; content length: " + req.getContentLength());
				InputStream in = req.getInputStream();		
				int bytesRead = 0;		
				int bytesToRead= req.getContentLength();
				byte[] payload = null;
				if (bytesToRead > 0) {
					payload = new byte[bytesToRead];
					while (bytesRead < bytesToRead) {
					  int result = in.read(payload, bytesRead, bytesToRead - bytesRead);
					  if (result == -1) {
						  break;
					  }
					  bytesRead += result;
					}
				}
				return payload;
			}
			return null;
	}

}
