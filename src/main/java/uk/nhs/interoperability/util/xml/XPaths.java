/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util.xml;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;

import uk.nhs.interoperability.util.Logger;

//import com.xmlsolutions.annotation.Requirement;

/**
 * The Class XPaths.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class XPaths {
	
	/** The xpath factory. */
	private static XPathFactory xpathFactory;
	
	static {
		try {
			xpathFactory = XPathFactory.newInstance(XPathConstants.DOM_OBJECT_MODEL);
		} catch (XPathFactoryConfigurationException e) {
			Logger.error("Could not create XPath factory", e);
		}
	}
	
	/** The Constant ROOT. */
	public static final String ROOT = "/";
    
    /** The Constant SOAP_BODY. */
    public static final String SOAP_BODY = ROOT + "SOAP:Envelope/SOAP:Body";
    
    /** Obtains the SOAP body contents - typically this will contain the ITK distribution envelope. */
    public static final String SOAP_BODY_CONTENT = SOAP_BODY + "/*[1]";
    
    /** The Constant SOAP_HEADER. */
    public static final String SOAP_HEADER = ROOT + "SOAP:Envelope/SOAP:Header";
    
    /** The Constant WSA_MSG_ID. */
    public static final String WSA_MSG_ID = SOAP_HEADER + "/wsa:MessageID";
    
    /** The Constant WSA_TO. */
    //@Requirement(traceTo={"WS-STD-03"})
    public static final String WSA_TO = SOAP_HEADER + "/wsa:To";
    
    /** The Constant WSA_FROM. */
    //@Requirement(traceTo={"WS-STD-03"})
    public static final String WSA_FROM = SOAP_HEADER + "/wsa:From/wsa:Address";
    
    /** The Constant WSA_REPLY_TO. */
    //@Requirement(traceTo={"WS-STD-03"})
    public static final String WSA_REPLY_TO = SOAP_HEADER + "/wsa:ReplyTo/wsa:Address";
    
    /** The Constant WSA_FAULT_TO. */
    //@Requirement(traceTo={"WS-STD-03"})
    public static final String WSA_FAULT_TO = SOAP_HEADER + "/wsa:FaultTo/wsa:Address";
    
    /** The Constant WSA_ACTION. */
    //@Requirement(traceTo={"WS-STD-03"})
    public static final String WSA_ACTION = SOAP_HEADER + "/wsa:Action";
    
    // Add in Xpaths for security header
    /** The Constant WSA_SECURITY_TIMESTAMP. */
    public static final String WSA_SECURITY_TIMESTAMP = SOAP_HEADER + "/wsse:Security/wsu:Timestamp";
    
    /** The Constant WSA_SECURITY_CREATED. */
    public static final String WSA_SECURITY_CREATED = SOAP_HEADER + "/wsse:Security/wsu:Timestamp/wsu:Created";
    
    /** The Constant WSA_SECURITY_EXPIRES. */
    public static final String WSA_SECURITY_EXPIRES = SOAP_HEADER + "/wsse:Security/wsu:Timestamp/wsu:Expires";
    
    /** The Constant WSA_SECURITY_USERNAME. */
    public static final String WSA_SECURITY_USERNAME = SOAP_HEADER + "/wsse:Security/wsse:UsernameToken/wsse:Username";
    
    /** Counts the number of payloads present in the message (does not rely on the count provided in the message itself). */
    public static final String ITK_PAYLOAD_COUNT = "count(itk:DistributionEnvelope/itk:payloads/itk:payload)";
    
    /** The Constant ITK_AUDIT_IDENTITY_URI. */
    public static final String ITK_AUDIT_IDENTITY_URI = "itk:DistributionEnvelope/itk:header/itk:auditIdentity/itk:id/@uri";
    
    /** The Constant ITK_AUDIT_IDENTITY_URI. */
    public static final String ITK_AUDIT_IDENTITY_TYPE = "itk:DistributionEnvelope/itk:header/itk:auditIdentity/itk:id/@type";

    /** The Constant ITK_FROM_ADDRESS. */
    public static final String ITK_FROM_ADDRESS = "itk:DistributionEnvelope/itk:header/itk:senderAddress/@uri";
    
    /** The Constant ITK_FIRST_TO_ADDRESS. */
    public static final String ITK_FIRST_TO_ADDRESS = "itk:DistributionEnvelope/itk:header/itk:addresslist/itk:address[1]/@uri";
    
    /** The Constant ITK_FIRST_MIMETYPE. */
    public static final String ITK_FIRST_MIMETYPE = "itk:DistributionEnvelope/itk:header/itk:manifest/itk:manifestitem[1]/@mimetype";

    /** The Constant ITK_FIRST_BASE64. */
    public static final String ITK_FIRST_BASE64 = "itk:DistributionEnvelope/itk:header/itk:manifest/itk:manifestitem[1]/@base64";
    
    
    
    /**
     * Obtains the profile id. Note this will take the profile id from the manifest for the manifest item. It is not
     * appropriate for a message containing multiple payloads
     */
    public static final String ITK_PROFILE_ID = "itk:DistributionEnvelope/itk:header/itk:manifest/itk:manifestitem[1]/@profileid";
    
    /**
     * The tracking id associated with the message. Used for end-to-end tracing and correlation
     */
    public static final String ITK_TRACKING_ID = "itk:DistributionEnvelope/itk:header/@trackingid";
    
    /**
     * XPath to extract the technical service from the message e.g. 
     * <code>urn:nhs-itk:services:201005:getNHSNumber-v1-0</code>
     */
    public static final String ITK_SERVICE = "itk:DistributionEnvelope/itk:header/@service";
    
    /** The Constant ITK_BUSINESS_ACK_HANDLING_SPECIFICATIONS. */
    public static final String ITK_BUSINESS_ACK_HANDLING_SPECIFICATIONS = "itk:DistributionEnvelope/itk:header/itk:handlingSpecification/itk:spec[@key='urn:nhs:itk:ns:201005:ackrequested']/@value";
    
    /** The Constant ITK_FIRST_PAYLOAD. */
    public static final String ITK_FIRST_PAYLOAD = "itk:DistributionEnvelope/itk:payloads/itk:payload[1]/*[1]";

    /** The Constant ITK_FIRST_PAYLOAD_TEXT. */
    public static final String ITK_FIRST_PAYLOAD_TEXT = "itk:DistributionEnvelope/itk:payloads/itk:payload[1]/text()";
    
    /** The Constant ITK_FIRST_PAYLOAD_ID. */
    public static final String ITK_FIRST_PAYLOAD_ID = "itk:DistributionEnvelope/itk:payloads/itk:payload[1]/@id";
    
    /** The Constant ITK_SIMPLE_MESSAGE_RESPONSE. */
    public static final String ITK_SIMPLE_MESSAGE_RESPONSE = "itk:SimpleMessageResponse";
    
    /** The Constant ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT. */
    public static final String ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT = "itk:SimpleMessageResponse/* | itk:SimpleMessageResponse/text()";
    
    /** The Constant SOAP_WRAPPED_ITK_FIRST_PAYLOAD. */
    public static final String SOAP_WRAPPED_ITK_FIRST_PAYLOAD = SOAP_BODY + "/" + ITK_FIRST_PAYLOAD;
    
    /** The Constant SOAP_WRAPPED_ITK_SIMPLE_MESSAGE_RESPONSE. */
    public static final String SOAP_WRAPPED_ITK_SIMPLE_MESSAGE_RESPONSE = SOAP_BODY + "/" + ITK_SIMPLE_MESSAGE_RESPONSE;
    
    //SOAP Fault XPaths
    /** The Constant WSA_SOAP_ERROR_DETAIL. */
    public static final String WSA_SOAP_ERROR_DETAIL = SOAP_BODY + "/SOAP:Fault/SOAP:detail/*[1]";
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_ID. */
    public static final String WSA_SOAP_ERROR_DETAIL_ID = "itk:itkErrorInfo/itk:ErrorID";
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_CODE. */
    public static final String WSA_SOAP_ERROR_DETAIL_CODE = "itk:itkErrorInfo/itk:ErrorCode";
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_TEXT. */
    public static final String WSA_SOAP_ERROR_DETAIL_TEXT = "itk:itkErrorInfo/itk:ErrorText";
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_DIAGNOSTIC. */
    public static final String WSA_SOAP_ERROR_DETAIL_DIAGNOSTIC = "itk:itkErrorInfo/itk:ErrorDiagnosticText";
    
    /** The Constant NS_CONTEXT. */
    public static final NamespaceContext NS_CONTEXT = new XMLNamespaceContext();	
    
    
    /** The Constant ROOT_XPATH. */
    public static final XPathExpression ROOT_XPATH = compileXPath(ROOT);
    
    /** The Constant WSA_MSGID_XPATH. */
    public static final XPathExpression WSA_MSGID_XPATH = compileXPath(WSA_MSG_ID);
    
    /** The Constant WSA_REPLY_TO_XPATH. */
    public static final XPathExpression WSA_REPLY_TO_XPATH = compileXPath(WSA_REPLY_TO);
    
    /** The Constant WSA_FAULT_TO_XPATH. */
    public static final XPathExpression WSA_FAULT_TO_XPATH = compileXPath(WSA_FAULT_TO);
    
    /** The Constant WSA_ACTION_XPATH. */
    public static final XPathExpression WSA_ACTION_XPATH = compileXPath(WSA_ACTION);
    
    /** The Constant WSA_FROM_XPATH. */
    public static final XPathExpression WSA_FROM_XPATH = compileXPath(WSA_FROM);
    
    /** The Constant WSA_TO_XPATH. */
    public static final XPathExpression WSA_TO_XPATH = compileXPath(WSA_TO);
    
    /** The Constant SOAP_HEADER_XPATH. */
    public static final XPathExpression SOAP_HEADER_XPATH = compileXPath(SOAP_HEADER);
    
    /** The Constant SOAP_BODY_XPATH. */
    public static final XPathExpression SOAP_BODY_XPATH = compileXPath(SOAP_BODY);
    
    /** The Constant SOAP_BODY_CONTENT_XPATH. */
    public static final XPathExpression SOAP_BODY_CONTENT_XPATH = compileXPath(SOAP_BODY_CONTENT);
    
    /** The Constant SOAP_WRAPPED_ITK_FIRST_PAYLOAD_XPATH. */
    public static final XPathExpression SOAP_WRAPPED_ITK_FIRST_PAYLOAD_XPATH = compileXPath(SOAP_WRAPPED_ITK_FIRST_PAYLOAD);
    
    /** The Constant SOAP_WRAPPED_ITK_SIMPLE_MESSAGE_RESPONSE_XPATH. */
    public static final XPathExpression SOAP_WRAPPED_ITK_SIMPLE_MESSAGE_RESPONSE_XPATH = compileXPath(SOAP_WRAPPED_ITK_SIMPLE_MESSAGE_RESPONSE);
    
    /** The Constant ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT_XPATH. */
    public static final XPathExpression ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT_XPATH = compileXPath(ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT);

    // Security headers
    /** The Constant WSA_SECURITY_TIMESTAMP_XPATH. */
    public static final XPathExpression WSA_SECURITY_TIMESTAMP_XPATH = compileXPath(WSA_SECURITY_TIMESTAMP);
    
    /** The Constant WSA_SECURITY_CREATED_XPATH. */
    public static final XPathExpression WSA_SECURITY_CREATED_XPATH = compileXPath(WSA_SECURITY_CREATED);
    
    /** The Constant WSA_SECURITY_EXPIRES_XPATH. */
    public static final XPathExpression WSA_SECURITY_EXPIRES_XPATH = compileXPath(WSA_SECURITY_EXPIRES);
    
    /** The Constant WSA_SECURITY_USERNAME_XPATH. */
    public static final XPathExpression WSA_SECURITY_USERNAME_XPATH = compileXPath(WSA_SECURITY_USERNAME);
    
    //The following compiled XPaths need the distribution envelope as the root node / evaluation context    
    /** The Constant ITK_PAYLOAD_COUNT_XPATH. */
    public static final XPathExpression ITK_PAYLOAD_COUNT_XPATH = compileXPath(ITK_PAYLOAD_COUNT);
    
    /** The Constant ITK_AUDIT_IDENTITY_URI_XPATH. */
    public static final XPathExpression ITK_AUDIT_IDENTITY_URI_XPATH = compileXPath(ITK_AUDIT_IDENTITY_URI);
    
    /** The Constant ITK_AUDIT_IDENTITY_TYPE_XPATH. */
    public static final XPathExpression ITK_AUDIT_IDENTITY_TYPE_XPATH = compileXPath(ITK_AUDIT_IDENTITY_TYPE);
    
    /** The Constant ITK_FROM_ADDRESS_XPATH. */
    public static final XPathExpression ITK_FROM_ADDRESS_XPATH = compileXPath(ITK_FROM_ADDRESS);
    
    /** The Constant ITK_FIRST_TO_ADDRESS_XPATH. */
    public static final XPathExpression ITK_FIRST_TO_ADDRESS_XPATH = compileXPath(ITK_FIRST_TO_ADDRESS);
    
    /** The Constant ITK_PROFILE_ID_XPATH. */
    public static final XPathExpression ITK_PROFILE_ID_XPATH = compileXPath(ITK_PROFILE_ID);
    
    /** The Constant ITK_SERVICE_XPATH. */
    public static final XPathExpression ITK_SERVICE_XPATH = compileXPath(ITK_SERVICE);
    
    /** The Constant ITK_BUSINESS_ACK_HANDLING_SPECIFICATIONS_XPATH. */
    public static final XPathExpression ITK_BUSINESS_ACK_HANDLING_SPECIFICATIONS_XPATH = compileXPath(ITK_BUSINESS_ACK_HANDLING_SPECIFICATIONS);
    
    /** The Constant ITK_TRACKING_ID_XPATH. */
    public static final XPathExpression ITK_TRACKING_ID_XPATH = compileXPath(ITK_TRACKING_ID);
    
    /** The Constant ITK_FIRST_PAYLOAD_XPATH. */
    public static final XPathExpression ITK_FIRST_PAYLOAD_XPATH = compileXPath(ITK_FIRST_PAYLOAD);
    
    /** The Constant ITK_FIRST_PAYLOAD_ID_XPATH. */
    public static final XPathExpression ITK_FIRST_PAYLOAD_ID_XPATH = compileXPath(ITK_FIRST_PAYLOAD_ID);
    
    /** The Constant ITK_FIRST_PAYLOAD_TEXT_XPATH. */
    public static final XPathExpression ITK_FIRST_PAYLOAD_TEXT_XPATH = compileXPath(ITK_FIRST_PAYLOAD_TEXT);

    /** The Constant ITK_FIRST_MIMETYPE_XPATH. */
    public static final XPathExpression ITK_FIRST_MIMETYPE_XPATH = compileXPath(ITK_FIRST_MIMETYPE);
    
    /** The Constant ITK_FIRST_BASE64_XPATH. */
    public static final XPathExpression ITK_FIRST_BASE64_XPATH = compileXPath(ITK_FIRST_BASE64);

    
    //The following compiled XPaths work for SOAP faults
    /** The Constant WSA_SOAP_ERROR_DETAIL_XPATH. */
    public static final XPathExpression WSA_SOAP_ERROR_DETAIL_XPATH = compileXPath(WSA_SOAP_ERROR_DETAIL);
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_ID_XPATH. */
    public static final XPathExpression WSA_SOAP_ERROR_DETAIL_ID_XPATH = compileXPath(WSA_SOAP_ERROR_DETAIL_ID);
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_CODE_XPATH. */
    public static final XPathExpression WSA_SOAP_ERROR_DETAIL_CODE_XPATH = compileXPath(WSA_SOAP_ERROR_DETAIL_CODE);
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_TEXT_XPATH. */
    public static final XPathExpression WSA_SOAP_ERROR_DETAIL_TEXT_XPATH = compileXPath(WSA_SOAP_ERROR_DETAIL_TEXT);
    
    /** The Constant WSA_SOAP_ERROR_DETAIL_DIAGNOSTIC_XPATH. */
    public static final XPathExpression WSA_SOAP_ERROR_DETAIL_DIAGNOSTIC_XPATH = compileXPath(WSA_SOAP_ERROR_DETAIL_DIAGNOSTIC);
    
	public static XPathExpression getXPathExpression(String xPath) {
		XPath xp = xpathFactory.newXPath();
		xp.setNamespaceContext(NS_CONTEXT);
		try {
			return xp.compile(xPath);
		} catch (XPathExpressionException e) {
			Logger.error("Error compiling XPath \"" + xPath + "\"", e);
		}
		return null;
	}
    
	
	/**
	 * Compile x path.
	 *
	 * @param xPath the x path
	 * @return the x path expression
	 */
	private static XPathExpression compileXPath(String xPath) {
		XPath xp = xpathFactory.newXPath();
		xp.setNamespaceContext(NS_CONTEXT);
		try {
			return xp.compile(xPath);
		} catch (XPathExpressionException e) {
			Logger.error("Error compiling XPath \"" + xPath + "\"", e);
		}
		return null;
	}
	
	

}
