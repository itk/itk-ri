/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

/**
 * The Class ITKApplicationProperties.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKApplicationProperties {

	/** The props. */
	private static Properties props = new Properties();
	static {
		try {
			props.load(ITKApplicationProperties.class.getResourceAsStream("/application.properties"));			
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Gets the property.
	 *
	 * @param propertyName the property name
	 * @return the property
	 */
	public static String getProperty(String propertyName){
		return props.getProperty(propertyName);
	}
	
	/**
	 * Gets the properties matching.
	 *
	 * @param prefix the prefix
	 * @return the properties matching
	 */
	public static Properties getPropertiesMatching(String prefix) {
		return getPropertiesMatching(props, prefix);
	}
	
	/**
	 * Gets the properties matching.
	 *
	 * @param props the props
	 * @param prefix the prefix
	 * @return the properties matching
	 */
	public static Properties getPropertiesMatching(Properties props, String prefix) {
		Properties matchingProperties = new Properties();
		Set<Entry<Object, Object>> entries = props.entrySet();
		for (Entry<Object, Object> entry: entries) {
			if (entry.getKey() instanceof String && ((String)entry.getKey()).startsWith(prefix)) {
				matchingProperties.setProperty((String)entry.getKey(), (String)entry.getValue());
			}
		}
		return matchingProperties;
	}

}