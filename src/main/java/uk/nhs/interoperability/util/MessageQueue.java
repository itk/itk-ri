/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.util;

import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;

/**
 * The Class MessageQueue.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class MessageQueue implements Runnable {
	
	/** The executor service. */
	private ExecutorService executorService;
	
	/** The is running. */
	private boolean isRunning = true;
	
	/** The itk message sender. */
	private ITKMessageSender itkMessageSender;
	
	/*
	 * Construct an in-memory queue to queue outbound ITK messages 
	 */
	/** The async processing queue. */
	private Queue<ITKMessage> asyncProcessingQueue = new LinkedBlockingQueue<ITKMessage>();
	
	/**
	 * Instantiates a new message queue.
	 */
	public MessageQueue() {
		this.executorService = Executors.newFixedThreadPool(1);
		this.executorService.execute(this);
		this.itkMessageSender = new ITKMessageSenderImpl();
	}

	/**
	 * Queue.
	 *
	 * @param request the request
	 */
	public void queue(ITKMessage request) {
		//Queue for asynchronous processing
		this.asyncProcessingQueue.add(request);
	}

	/**
	 * Process async message.
	 *
	 * @param request the request
	 */
	private void processAsyncMessage(ITKMessage request) {
		Logger.debug("Processing queued itk request");
		try {
			this.itkMessageSender.send(request);
		} catch (ITKMessagingException e) {
			/*
			 * In a real application some more in-depth error
			 * handling may occur such as storing the failed message
			 * for attention of an administrator, however for this
			 * simple application emulator we just log an error
			 */
			Logger.error("Could not send aysnchronous request", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (this.isRunning) {
			try {
				if (this.asyncProcessingQueue.isEmpty()) {
					Thread.sleep(5000);
				} else {
					this.processAsyncMessage(this.asyncProcessingQueue.poll());
				}
			} catch (InterruptedException e) {
				this.isRunning = false;
			}
		}
	}

}
