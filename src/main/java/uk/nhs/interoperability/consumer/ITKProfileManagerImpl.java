/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//import com.xmlsolutions.annotation.Requirement;

import uk.nhs.interoperability.capabilities.ITKProfileManager;
import uk.nhs.interoperability.util.ITKApplicationProperties;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class ITKProfileManagerImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKProfileManagerImpl implements ITKProfileManager {
	
	/** The Constant SUPPORTED_PROFILE_PREFIX. */
	private static final String SUPPORTED_PROFILE_PREFIX = "profileId.";
	
	/** The supported profiles. */
	private Map<String, Integer> supportedProfiles;

	/**
	 * Instantiates a new iTK profile manager impl.
	 */
	public ITKProfileManagerImpl() {
		Properties props = new Properties();
		try {
			props.load(this.getClass().getResourceAsStream("/profile.properties"));
			this.loadProps(props);
		} catch (IOException e) {
			Logger.error("Could not load supported profile configuration - no profiles will be supported", e);
		}
		
	}
	
	/**
	 * Instantiates a new iTK profile manager impl.
	 *
	 * @param props the props
	 */
	public ITKProfileManagerImpl(Properties props) {
		this.loadProps(props);
	}
	
	/**
	 * Load props.
	 *
	 * @param props the props
	 */
	private void loadProps(Properties props) {
		this.supportedProfiles = new HashMap<String, Integer>();
		try {
			//Read in supported profile from properties
			Properties profileProps = ITKApplicationProperties.getPropertiesMatching(props, SUPPORTED_PROFILE_PREFIX);
			for (Map.Entry<Object, Object> entry : profileProps.entrySet()) {
				String profileId = entry.getKey().toString().substring(SUPPORTED_PROFILE_PREFIX.length());
				String supportLevelStr = entry.getValue().toString();
				if (supportLevelStr.equalsIgnoreCase("ACCEPTED")) {
					this.supportedProfiles.put(profileId, new Integer(ITKProfileManager.ACCEPTED));
				} else if (supportLevelStr.equalsIgnoreCase("DEPRECATED")) {
					this.supportedProfiles.put(profileId, new Integer(ITKProfileManager.DEPRECATED));
				} else {
					//By default set to not supported
					this.supportedProfiles.put(profileId, new Integer(ITKProfileManager.NOT_SUPPORTED));
				}
			}
		} catch (Throwable t) {
			Logger.fatal("Error loading supported profiles information, no profiles will be supported!", t);
		}
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.capabilities.ITKProfileManager#getProfileSupportLevel(java.lang.String)
	 */
	@Override
	//@Requirement(traceTo={"COR-DEH-09"}, status="implemented")
	public int getProfileSupportLevel(String profileId) {
		return this.supportedProfiles.containsKey(profileId) ? this.supportedProfiles.get(profileId).intValue() : ITKProfileManager.NOT_SUPPORTED;
	}

}
