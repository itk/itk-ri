/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.capabilities.AuditException;
import uk.nhs.interoperability.capabilities.AuditService;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.infrastructure.ITKTransportPropertiesImpl;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.ITKSimpleMessageResponse;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.service.ITKSimpleAudit;
import uk.nhs.interoperability.transport.ITKTransportProperties;
import uk.nhs.interoperability.transport.WS.ITKSOAPException;
import uk.nhs.interoperability.transport.WS.WSSOAPMessageImpl;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.ServletUtils;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;

/**
 * The Class AbstractRoutedMessageServlet.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
@SuppressWarnings("serial")
public abstract class AbstractRoutedMessageServlet extends ITKServlet {
	
	//Reference to the message consumer that will handle the request
	/** The message consumer. */
	private ITKMessageConsumer messageConsumer;
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.ITKServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		this.messageConsumer = getMessageConsumer();
		super.init();
	}
	
	/**
	 * Gets the message consumer.
	 *
	 * @return the message consumer
	 */
	public abstract ITKMessageConsumer getMessageConsumer();

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Logger.debug("Received a routed message"); 
		
		try {
			byte[] requestMsgBytes = ServletUtils.readRequestContent(req);
			if (requestMsgBytes == null) {
				throw new ITKMessagingException(ITKMessagingException.INVALID_MESSAGE_CODE, "The request did not contain any content");
			}
			String requestString = new String(requestMsgBytes);
			ITKMessage requestMsg = this.createRequestMessage(requestString);
			ITKMessage responseMsg = this.processMessage(requestMsg);

			//Now create and Send InfrastructureAck (note this could be handle by another thread - done inline for simplicity)
			this.sendInfrastructureAck(requestMsg.getMessageProperties());

			//Send back the returned message having added the SOAP wrappers
			// No destination object is needed to respond on the same channel
			ITKMessage response = new WSSOAPMessageImpl(null, responseMsg, WSSOAPMessageImpl.SYNCRESP);

			resp.getWriter().write(response.getFullPayload());
			
			return;
		} catch (ITKMessagingException e) {
			Logger.error("Could not process message", e);
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			/*
			 * Raise a SOAP fault - if this is a multi-hop assume
			 * intermediary will convert SOAP Fault into Infrastructure NACK
			 */
			resp.getWriter().write(new ITKSOAPException(e).serialiseXML());
		} catch (Throwable t) {
			Logger.error("Could not process message - general error", t);
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			resp.getWriter().write(t.getLocalizedMessage());
		} finally {
			Logger.debug("Completed processing");
		}
	}
	
	/**
	 * Creates the request message.
	 *
	 * @param requestString the request string
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage createRequestMessage(String requestString) throws ITKMessagingException {
		ITKTransportProperties itkTransportProperties = null;
		ITKMessagePropertiesImpl itkMessageProperties = null;
		try {

			// 1) PROCESS THE REQUEST DOCUMENT
			//Parse the request
			Document doc = DomUtils.parse(requestString);
			//Pretty print the request message
			Logger.trace(DomUtils.serialiseToXML(doc, DomUtils.PRETTY_PRINT));
			
			// 2) PROCESS THE SOAP HEADERS
			//Extract the transport properties
			itkTransportProperties = ITKTransportPropertiesImpl.buildFromSoap(doc);

			// 3) PROCESS THE DISTRIBUTION ENVELOPE
			//Extract the distribution envelope
			Document de = ITKMessagePropertiesImpl.extractDistributionEnvelopeFromSoap(doc);
			//Extract message properties from the request
			itkMessageProperties = (ITKMessagePropertiesImpl) ITKMessagePropertiesImpl.build(de);
			// Validate the message properties
			this.validateDistributionEnvelope(itkMessageProperties);
			//Attach the associated transport properties so that they are available for asynchronous invocations
			itkMessageProperties.setInboundTransportProperties(itkTransportProperties);
			
			
			// 4) PROCESS THE PAYLOAD
			//Obtain the actual business payload
			Document businessPayload = DomUtils.createDocumentFromNode((Node)XPaths.SOAP_WRAPPED_ITK_FIRST_PAYLOAD_XPATH.evaluate(doc, XPathConstants.NODE));
			//Show the extracted payload in the trace log
			Logger.trace(DomUtils.serialiseToXML(businessPayload, DomUtils.PRETTY_PRINT));
			
			//Construct the appropriate object for handing over to the application
			ITKMessage requestMsg = new SimpleMessage();
			requestMsg.setBusinessPayload(DomUtils.serialiseToXML(businessPayload));
			requestMsg.setMessageProperties(itkMessageProperties);
			
			return requestMsg;
			
		} catch (XPathExpressionException e) {
			throw new ITKMessagingException(itkTransportProperties, itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not extract values from request", e);
		} catch (IOException e) {
			throw new ITKMessagingException(itkTransportProperties, itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not parse request", e);
		} catch (ParserConfigurationException e) {
			throw new ITKMessagingException(itkTransportProperties, itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "XML parser configuration error", e);
		} catch (SAXException e) {
			throw new ITKMessagingException(itkTransportProperties, itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Error parsing request XML", e);
		}
	}
	
	/**
	 * Process message.
	 *
	 * @param requestMsg the request msg
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage processMessage(ITKMessage requestMsg) throws ITKMessagingException {
		ITKMessageProperties itkMessageProperties = requestMsg.getMessageProperties();
		ITKTransportProperties itkTransportProperties = itkMessageProperties.getInboundTransportProperties();
		try {
			//Pass the routed message to the application
			this.messageConsumer.onMessage(requestMsg);
				
			//Audit receipt event
			ITKSimpleAudit.getInstance().auditEvent(AuditService.MESSAGE_RECEIPT_EVENT, System.currentTimeMillis(), itkMessageProperties);
				
			//Create and return simple message response
			return new ITKSimpleMessageResponse(itkMessageProperties, true);
			
		} catch (AuditException e) {
			throw new ITKMessagingException(itkTransportProperties, itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_RETRYABLE_CODE, "Failed to write audit", e);
		} 
	}
	

}
