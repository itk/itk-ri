/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import uk.nhs.interoperability.capabilities.ITKProfileManager;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.DEWrappedMessage;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.ITKSimpleMessageResponse;
import uk.nhs.interoperability.service.ITKService;
import uk.nhs.interoperability.service.ITKSimpleDOS;
import uk.nhs.interoperability.transport.ITKInfrastructureAck;
import uk.nhs.interoperability.util.ITKApplicationProperties;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.MessageQueue;

/**
 * The Class ITKServlet.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
@SuppressWarnings("serial")
public abstract class ITKServlet extends HttpServlet {
	
	/** The profile manger. */
	private ITKProfileManager profileManger;
	//private ITKMessageSender itkMessageSender;
	/** The message queue. */
	private MessageQueue messageQueue;
	
	/** The audit identity. */
	private String auditIdentity;
	
	/** The dos. */
	protected ITKSimpleDOS dos;
	
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		try {
			this.dos = new ITKSimpleDOS();
			this.messageQueue = new MessageQueue();
			this.profileManger = new ITKProfileManagerImpl();
			//this.itkMessageSender = new ITKMessageSenderImpl();
			this.auditIdentity = ITKApplicationProperties.getProperty("audit.identity");
		} catch (Throwable t) {
			throw new ServletException("Could not instantiate ITKMessageConsumer instance", t);
		}
		super.init();
	}
	
	
	/**
	 * Send infrastructure ack.
	 *
	 * @param requestMessageProperties the request message properties
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	protected void sendInfrastructureAck(ITKMessageProperties requestMessageProperties) throws ITKMessagingException {
		Logger.debug("Creating and sending an infrastructureAck");
		ITKMessage ack = new ITKInfrastructureAck(requestMessageProperties, this.auditIdentity);
		//this.itkMessageSender.sendAsync(ack);
		this.messageQueue.queue(ack);
	}
	
	/**
	 * Checks whether the profileId in as identified in the ITKMessagingProperties is
	 * supported by the recipient.
	 *
	 * @param itkMessageProperties The ITKMessageProperties holding a reference to the
	 * profileId of the message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	protected void checkProfileId(ITKMessageProperties itkMessageProperties) throws ITKMessagingException {
		if (itkMessageProperties != null && itkMessageProperties.getProfileId() != null) {
			String profileId = itkMessageProperties.getProfileId();
			int profileSupportLevel = this.profileManger.getProfileSupportLevel(profileId);
			if (profileSupportLevel == ITKProfileManager.DEPRECATED) {
				Logger.warn("Profile \"" + profileId + "\" is deprecated - it will be processed for now but sender should be informed that support may be withdrawn in the near future");
			} else if (profileSupportLevel != ITKProfileManager.ACCEPTED) {
				Logger.warn("Profile \"" + profileId + "\" is not accepted - message will be rejected");
				throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.INVALID_MESSAGE_CODE, "Profile \"" + profileId + "\" is not supported - rejecting message " + itkMessageProperties.getBusinessPayloadId());
			}
			//Profile is accepted
			return;
		}
		throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.INVALID_MESSAGE_CODE, "Request message profileId could not be determined - rejecting message");
	}
	
	/**
	 * Check service id.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	protected void checkServiceId(ITKMessageProperties itkMessageProperties) throws ITKMessagingException {
		String serviceId = itkMessageProperties.getServiceId();
		ITKService service = dos.getService(serviceId);
		if (service==null){
			Logger.warn("Service \"" + serviceId + "\" is not accepted - message will be rejected");
			throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, 
					"Service \"" + serviceId + "\" is not supported - rejecting message " + itkMessageProperties.getBusinessPayloadId());
		}
	}


	/**
	 * Validate distribution envelope.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	protected void validateDistributionEnvelope(ITKMessageProperties itkMessageProperties)
			throws ITKMessagingException {
			
				//Check that the profileId is accepted by the recipient application
				this.checkProfileId(itkMessageProperties);
				
				//Check that the serviceId is accepted by the recipient application
				this.checkServiceId(itkMessageProperties);
				
				if (itkMessageProperties.getAuditIdentity().getURI().length()==0) {
					Logger.warn("Audit Identity not populated - message will be rejected");
					throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.INVALID_MESSAGE_CODE, 
							"Audit Identity not populated - message will be rejected" );
				}
				if (itkMessageProperties.getAuditIdentity().getURI().contains("INVALID")) {
					Logger.warn("Audit Identity not populated - message will be rejected");
					throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.INVALID_MESSAGE_CODE, 
							"Audit Identity not populated - message will be rejected" );
				}
				
				// CM_DE_003: Check that the businessPayloadId is present.
				if (itkMessageProperties.getBusinessPayloadId().length()==0) {
					Logger.warn("Business Payload Id not populated - message will be rejected");
					throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.INVALID_MESSAGE_CODE, 
							"Business Payload Id not populated - message will be rejected" );
			
				}
			}


	/**
	 * Adds the itk wrappers.
	 *
	 * @param itkMessage the itk message
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	protected ITKMessage addITKWrappers(ITKMessage itkMessage) throws ITKMessagingException {
		
		ITKMessage wrappedMessage = itkMessage ;
		if (itkMessage != null && !(itkMessage instanceof ITKSimpleMessageResponse)) {
			//Wrap in DE
			//itkMessage = new DEWrappedMessage(itkMessage);
	
			// Get the ITKService from the DirectoryOfService
			ITKService service = dos.getService(itkMessage.getMessageProperties().getServiceId());
			if (service==null){
				Logger.warn("Response Service not configured - message will be rejected");
				throw new ITKMessagingException(ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, 
						"Response Service not configured - message will be rejected" );
			}
	
			//if (route.getWrapperType().equals(ITKTransportRoute.DISTRIBUTION_ENVELOPE)){
				Logger.trace("Adding distribution envelope wrapper");
				if(service.isBase64()){
					String b64DocText = javax.xml.bind.DatatypeConverter
							.printBase64Binary(itkMessage.getBusinessPayload().getBytes());
					itkMessage.setBusinessPayload(b64DocText);
				}
				wrappedMessage = new DEWrappedMessage(service, itkMessage, true); 
			//} else {
			//	Logger.trace("No DE wrapper required");
			//	wrappedMessage = itkMessage;
			//}
		}	
		return wrappedMessage;
	}

}
