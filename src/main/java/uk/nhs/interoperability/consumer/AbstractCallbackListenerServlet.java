/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.infrastructure.ITKCommsException;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKCallbackHandler;
import uk.nhs.interoperability.transport.WS.ITKSOAPException;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.ServletUtils;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;

//import com.xmlsolutions.annotation.Requirement;

/**
 *
 * @author Nick Jones
 * @author Mike Odling-Smee
 * @since 0.1
 * 
 * This abstract class can be extended by a user written class which must implement the abstract method getCallbackHandler.
 * 
 * See the samples project for examples of how this may be achieved.
 */
@SuppressWarnings("serial")
public abstract class AbstractCallbackListenerServlet extends ITKServlet {

	//Reference to the message consumer that will handle the request
	/** The callback handler. */
	private ITKCallbackHandler callbackHandler;
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.ITKServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		this.callbackHandler = getCallbackHandler();
		super.init();
	}
	
	/**
	 * Gets the callback handler.
	 *
	 * @return the callback handler
	 */
	public abstract ITKCallbackHandler getCallbackHandler();
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	//@Requirement(traceTo={"WS-PAT-02"})
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Logger.debug("Received a message"); 
		String requestString = new String(ServletUtils.readRequestContent(req));
		try {
			this.processSOAPMessage(requestString);
			resp.setStatus(HttpServletResponse.SC_ACCEPTED);
			Logger.debug("Completed processing");
			return;
		} catch (ITKMessagingException e) {
			Logger.error("Could not process Callback message", e);
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			resp.getWriter().write(new ITKSOAPException(e).serialiseXML());
		} catch (Throwable t) {
			Logger.error("Could not process Callback message - general error", t);
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			resp.getWriter().write(t.getLocalizedMessage());
		}
	}
	
	// This method is responsible for processing the SOAP message before handing off to the ITK Layer.
	// (Mirrors ITKSenderWSImpl)
	/**
	 * Process soap message.
	 *
	 * @param requestString the request string
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	//@Requirement(traceTo={"WS-PAT-02"})
	private void processSOAPMessage(String requestString) throws ITKMessagingException {	

		ITKMessageProperties itkMessageProperties = null;
		ITKMessage requestMsg = null;

		try {
			//Parse the request
			Document requestDocument = DomUtils.parse(requestString);

			if (requestDocument == null){
				
				// No requestDocument means the servlet received no content - this is an exception.
				ITKMessagingException unknownRequestException = new ITKMessagingException(
						ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "No Content Received.");
				Logger.error("Unknown request type",unknownRequestException);
				throw unknownRequestException;
				
			} else {
				
				// Pretty print the request message
				Logger.trace(DomUtils.serialiseToXML(requestDocument, DomUtils.PRETTY_PRINT));

				// Extract the SOAP Body Content
				Document businessPayloadDocument = DomUtils.createDocumentFromNode((Node)XPaths.SOAP_BODY_CONTENT_XPATH.evaluate(requestDocument, XPathConstants.NODE));

				if (businessPayloadDocument == null){
					// exception
					ITKMessagingException unknownRequestException = new ITKMessagingException(
							ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "No payload in SOAP Body");
					Logger.error("Unknown request type",unknownRequestException);
					throw unknownRequestException;
				}
				
				String requestPayloadString = DomUtils.serialiseToXML(businessPayloadDocument);
				
				//Construct the appropriate object for handing over to the application
				requestMsg = new SimpleMessage();
				requestMsg.setBusinessPayload(requestPayloadString);
				
				processITKMessage(requestMsg);

			}
	
		} catch (XPathExpressionException e) {
			throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not extract values from request", e);
		} catch (IOException e) {
			throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Could not parse request", e);
		} catch (ParserConfigurationException e) {
			throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "XML parser configuration error", e);
		} catch (SAXException e) {
			throw new ITKMessagingException(itkMessageProperties, ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Error parsing request XML", e);
		}
	}
	
	// This method is responsible for processing the ITK message before handing off to the app.
	// (Mirrors ITKMessageSenderImpl)
	/**
	 * Process itk message.
	 *
	 * @param requestMessage the request message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private void processITKMessage(ITKMessage requestMessage) throws ITKMessagingException {	

		try {
			
			Document requestDocument = DomUtils.parse(requestMessage.getBusinessPayload());
			String requestPayloadName = requestDocument.getDocumentElement().getLocalName();
			Logger.trace("Received:"+requestPayloadName);
			
			if (requestPayloadName.equalsIgnoreCase("DistributionEnvelope")){
				
				Logger.trace("Processing DistributionEnvelope");
				processDistributionEnvelope(requestDocument);
				
			} else if (requestPayloadName.equalsIgnoreCase("SimpleMessageResponse")){
				
				Logger.trace("Processing SimpleMessageResponse");
				
				ITKMessage applicationMessage = new SimpleMessage();
				String payload = (String)XPaths.ITK_SIMPLE_MESSAGE_RESPONSE_CONTENT_XPATH.evaluate(requestDocument);	
				applicationMessage.setBusinessPayload(payload);

				// Set up a dummy MessageProperties for the audit process. There is no audit identifier on a Simple Message Response
				ITKMessageProperties itkMessageProperties = new ITKMessagePropertiesImpl();
				itkMessageProperties.setAuditIdentity(new ITKIdentityImpl("NOT SPECIFIED"));
				applicationMessage.setMessageProperties(itkMessageProperties);
				
				this.callbackHandler.onMessage(applicationMessage);
				
			} else {
				
				Logger.trace("Processing UNKNOWN");
				ITKMessagingException unknownResponseException = new ITKMessagingException(
						ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "Neither DistributionEnvelope nor SimpleMessageResponse Found");
				Logger.error("Unknown response type",unknownResponseException);
				throw unknownResponseException;
				
			}
			
		} catch (SAXException se) {
			Logger.error("SAXException processing response from Transport", se);
			throw new ITKCommsException("XML Error Processing ITK Response");
		} catch (IOException ioe) {
			Logger.error("IOException on Transport", ioe);
			throw new ITKCommsException("Transport error sending ITK Message");
		} catch (ParserConfigurationException pce) {
			Logger.error("ParseConfigurationException on Transport", pce);
			throw new ITKCommsException("XML Configuration Error Processing ITK Response");
		} catch (XPathExpressionException xpe) {
			Logger.error("XPathExpressionException reading payload on Transport Response", xpe);
			throw new ITKCommsException("No Payload found in ITK Response");
		}
		
	}

	/**
	 * Process distribution envelope.
	 *
	 * @param distributionEnvelope the distribution envelope
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private void processDistributionEnvelope(Document distributionEnvelope) throws ITKMessagingException {	

		ITKMessage applicationMessage = new SimpleMessage();
		
		try {
			
			// Extract message properties from the distribution envelope
			ITKMessageProperties itkMessageProperties = ITKMessagePropertiesImpl.build(distributionEnvelope);
			applicationMessage.setMessageProperties(itkMessageProperties);
	
			// Check plain payload
			String mimetype = (String)XPaths.ITK_FIRST_MIMETYPE_XPATH.evaluate(distributionEnvelope);
			Logger.debug("MimeType: " + mimetype);
			
			if (mimetype.equalsIgnoreCase("text/plain")){
				
				// Get the payload as a text node
				String payload = (String)XPaths.ITK_FIRST_PAYLOAD_TEXT_XPATH.evaluate(distributionEnvelope);
				
				// Check base64 encoding
				String base64 = (String)XPaths.ITK_FIRST_BASE64_XPATH.evaluate(distributionEnvelope);
				Logger.debug("Base64: " + base64);
				if (base64.equalsIgnoreCase("true")){
					byte[] payloadBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(payload);
					applicationMessage.setBusinessPayload(new String(payloadBytes));
				} else {
					applicationMessage.setBusinessPayload(payload);
				}
				this.callbackHandler.onMessage(applicationMessage);
				
			} else {
				// This is an XML payload
				// Get the first payload from the distribution envelope
				Document businessPayloadDocument = DomUtils.createDocumentFromNode((Node)XPaths.ITK_FIRST_PAYLOAD_XPATH.evaluate(distributionEnvelope, XPathConstants.NODE));
				
				if (businessPayloadDocument == null){
					// exception
					ITKMessagingException unknownResponseException = new ITKMessagingException(
							ITKMessagingException.PROCESSING_ERROR_NOT_RETRYABLE_CODE, "No payload in Distribution Envelope");
					Logger.error("Unknown response type",unknownResponseException);
					throw unknownResponseException;
				}
	
				String businessPayloadName = distributionEnvelope.getDocumentElement().getLocalName();
				Logger.trace("ITK Payload 1:"+businessPayloadName);
				applicationMessage.setBusinessPayload(DomUtils.serialiseToXML(businessPayloadDocument));
				
				if (businessPayloadName.equalsIgnoreCase("InfrastructureResponse")){
					Logger.trace("Processing InfrastructureResponse");
					
					// For an Infrastructure Acknowledgement, check if it is a ACK or a NACK, extract the pertinent details and pass
					// as an ITKAckDetails object to the application defined handlers
	
					String result = distributionEnvelope.getDocumentElement().getAttribute("result");
					if (result.equals("OK")){
						// Pass to APP for correlation etc.
						//TODO - Build the Acknowledgement
						this.callbackHandler.onAck(null);
					} else {
						//TODO - Build the Acknowledgement
						this.callbackHandler.onNack(null);
					}
					
				} else if (businessPayloadName.equalsIgnoreCase("BusinessResponseMessage")){
					
					Logger.trace("Processing BusinessResponseMessage (BusinessACK)");
					// A Business Acknowledgement is handled by the application layer and requires an Infrastructure Acknowledgement as it
					// is a Routed message
			
					// Call the application defined message handler.
					// Note: Exceptions in the handler will bubble up to the doPost where a SOAP or http exception will result
					this.callbackHandler.onMessage(applicationMessage);    
				
					// Now create and Send InfrastructureAck
					this.sendInfrastructureAck(applicationMessage.getMessageProperties());
					
				}	else {
					
					// Normal business message - send to the application
					this.callbackHandler.onMessage(applicationMessage);
	
				}
			}
		} catch (ITKMessagingException e){
			
		} catch (XPathExpressionException xpe) {
			Logger.error("XPathExpressionException reading DE on Transport Response", xpe);
			throw new ITKCommsException("Error processing Distribution Envelope");
		} catch (ParserConfigurationException pce) {
			Logger.error("ParseConfigurationException on DE", pce);
			throw new ITKCommsException("XML Configuration Error Processing Distribution Envelope");
		}
		
	}
	
}
