/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payload;

import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.transport.ITKTransportProperties;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class ITKMessageImpl.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public abstract class ITKMessageImpl implements ITKMessage {

	/** The message properties. */
	protected ITKMessageProperties messageProperties;
	
	/** The business payload. */
	protected String businessPayload;
	
	/** The is response. */
	protected boolean isResponse;
	
	/** The pre resolved transport route. */
	protected ITKTransportRoute preResolvedTransportRoute;

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getBusinessPayload()
	 */
	public String getBusinessPayload() {
		return businessPayload;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#setBusinessPayload(java.lang.String)
	 */
	public void setBusinessPayload(String businessPayload) {
		this.businessPayload = businessPayload;
	}
	
	/**
	 * Instantiates a new iTK message impl.
	 */
	public ITKMessageImpl() {
		
	}
	
	/**
	 * Instantiates a new iTK message impl.
	 *
	 * @param itkMessageProperties the itk message properties
	 */
	public ITKMessageImpl(ITKMessageProperties itkMessageProperties) {
		this(itkMessageProperties, null, null, false);
	}
	
	/**
	 * Instantiates a new iTK message impl.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @param auditIdentity the audit identity
	 * @param profileId the profile id
	 * @param reflectMessageProperties the reflect message properties
	 */
	public ITKMessageImpl(ITKMessageProperties itkMessageProperties, String auditIdentity, String profileId, boolean reflectMessageProperties) {
		if (reflectMessageProperties) {
			//Reflect the messageProperties as we assume they have come from the request
			ITKMessageProperties msgProperties = new ITKMessagePropertiesImpl();
			msgProperties.setAuditIdentity(new ITKIdentityImpl(auditIdentity));
			//Reflect some values from the request
			msgProperties.setFromAddress(itkMessageProperties.getToAddress());
			msgProperties.setToAddress(itkMessageProperties.getFromAddress());
			String responseServiceId = itkMessageProperties.getServiceId() + "Response";
			msgProperties.setServiceId(responseServiceId);
			msgProperties.setProfileId(profileId);
			//Attach the transport properties from the corresponding request
			msgProperties.setInboundTransportProperties(itkMessageProperties.getInboundTransportProperties());
			//If transport properties are not null set up the pre-resolved transport route
			ITKTransportProperties itkTransportProperties = itkMessageProperties.getInboundTransportProperties();
			if (itkTransportProperties != null) {
				Logger.trace("Obtaining transport route from inbound transport properties");
				this.preResolvedTransportRoute = itkMessageProperties.getInboundTransportProperties().getTransportReplyToRoute();
				if (preResolvedTransportRoute != null) {
					/*
					 * TODO - this perhaps should be driven by configuration but by default setting it so that Distribution
					 * Envelope is added to the message when being sent.
					 */
					this.preResolvedTransportRoute.setWrapperType(ITKTransportRoute.DISTRIBUTION_ENVELOPE);
					Logger.trace("Pre-resolved route: " + this.preResolvedTransportRoute);
				}
			}
			this.messageProperties = msgProperties;
			this.setIsReponse(true);
		} else {
			this.messageProperties = itkMessageProperties;
		}
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getMessageProperties()
	 */
	public ITKMessageProperties getMessageProperties() {
		return messageProperties;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#setMessageProperties(uk.nhs.interoperability.infrastructure.ITKMessageProperties)
	 */
	public void setMessageProperties(ITKMessageProperties messageProperties) {
		this.messageProperties = messageProperties;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getPreresolvedRoute()
	 */
	@Override
	public ITKTransportRoute getPreresolvedRoute() {
		return this.preResolvedTransportRoute;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#isResponse()
	 */
	@Override
	public boolean isResponse() {
		return this.isResponse;
	}
	
	/**
	 * Sets the checks if is reponse.
	 *
	 * @param isReponse the new checks if is reponse
	 */
	public void setIsReponse(boolean isReponse) {
		this.isResponse = isReponse;
	}
}
