/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payload;

import uk.nhs.interoperability.infrastructure.ITKMessageProperties;

/**
 * The Class SimpleMessage.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class SimpleMessage extends ITKMessageImpl {

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getFullPayload()
	 */
	public String getFullPayload() {
		return super.getBusinessPayload();
	}

	/**
	 * Instantiates a new simple message.
	 */
	public SimpleMessage() {
		super();
	}
	
	public SimpleMessage(String businessPayload) {
		this(null, businessPayload);
	}
	
	public SimpleMessage(ITKMessageProperties itkMessageProperties, String businessPayload) {
		super();
		this.setMessageProperties(itkMessageProperties);
		this.setBusinessPayload(businessPayload);
	}

	/**
	 * Instantiates a new simple message.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @param auditIdentity the audit identity
	 * @param profileId the profile id
	 * @param reflectMessageProperties the reflect message properties
	 */
	public SimpleMessage(ITKMessageProperties itkMessageProperties, String auditIdentity, String profileId, boolean reflectMessageProperties) {
		super(itkMessageProperties, auditIdentity, profileId, reflectMessageProperties);
	}

	/**
	 * Instantiates a new simple message.
	 *
	 * @param itkMessageProperties the itk message properties
	 */
	public SimpleMessage(ITKMessageProperties itkMessageProperties) {
		super(itkMessageProperties);
	}

}
