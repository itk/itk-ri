/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payload;

import java.util.Map;
import java.util.UUID;

import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.service.ITKService;
import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.util.Logger;

//import com.xmlsolutions.annotation.Requirement;

/**
 * The Class DEWrappedMessage.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class DEWrappedMessage extends ITKMessageImpl {
	
	/**
	 * Constant to indicate that the on the wire Distribution
	 * envelope XML contains logical originating and destination
	 * addresses
	 */
	public static final boolean DE_ADDRESSED = true;
	
	/**
	 * Constant to indicate that the on-the-wire Distribution
	 * envelope XML omits logical addressing (for instance
	 * this is not required in the case of synchronous calls)
	 */
	public static final boolean DE_UNADDRESSED = false;
	
	/** The tracking id. */
	private String trackingId;
	
	/**
	 * Whether or not the serialised XML Distribution Envelope contains logical addressing.
	 * Note this is on strictly required for a routed message 
	 * */
	private boolean addressed;
	
	/** The is base64. */
	private boolean isBase64;
	
	/** The mime type. */
	private String mimeType;
	
	/**
	 * Instantiates a new dE wrapped message.
	 */
	//@Requirement(traceTo="COR-DEH-02", status="implemented")
	public DEWrappedMessage() {
		//Note UUID must be in upper case with no prefix
		this.trackingId = UUID.randomUUID().toString().toUpperCase();
	}
	
	/**
	 * Instantiates a new dE wrapped message.
	 *
	 * @param message the message
	 */
	public DEWrappedMessage(ITKMessage message){
		this();
		this.setBusinessPayload(message.getBusinessPayload());
		this.messageProperties = message.getMessageProperties();
		this.addressed = true;
	}
	
	/**
	 * Instantiates a new dE wrapped message.
	 *
	 * @param service the service
	 * @param message the message
	 * @param addressed the addressed
	 */
	public DEWrappedMessage(ITKService service, ITKMessage message, boolean addressed){
		this();
		this.setBusinessPayload(message.getBusinessPayload());
		this.messageProperties = message.getMessageProperties();
		this.addressed = addressed;
		this.isBase64 = service.isBase64();
		this.mimeType = service.getMimeType();
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getFullPayload()
	 */
	//@Requirement(traceTo={"COR-DEH-01", "COR-DEH-04"}, status="implemented")
	public String getFullPayload() throws ITKMessagingException {
		String fullPayload = "";
		
		String deXML = "<ITKMessage>";
		deXML += "<Service>"+this.messageProperties.getServiceId()+"</Service>";
		deXML += "<TrackingId>" + this.trackingId + "</TrackingId>";
		deXML += "<Sender>"+this.messageProperties.getFromAddress().getURI()+"</Sender>";
		deXML += "<SenderType>"+this.messageProperties.getFromAddress().getType()+"</SenderType>";
		deXML += "<Recipient>"+this.messageProperties.getToAddress().getURI()+"</Recipient>";
		deXML += "<RecipientType>"+this.messageProperties.getToAddress().getType()+"</RecipientType>";
		deXML += "<Author>"+this.messageProperties.getAuditIdentity().getURI()+"</Author>";
		deXML += "<AuthorType>"+this.messageProperties.getAuditIdentity().getType()+"</AuthorType>";
		deXML += "<Manifest id=\""+this.messageProperties.getBusinessPayloadId()+
					"\" type=\""+this.mimeType+"\""+
				    " profileid=\""+this.messageProperties.getProfileId()+"\" ";
		if (this.isBase64){
			deXML += " base64=\"true\" ";
		}
		deXML += " />";
		//Add handling specifications if set
		if (!this.getMessageProperties().getHandlingSpecifications().isEmpty()) {
			deXML += "<HandlingSpecs>";
			for (Map.Entry<String, String> entry: this.getMessageProperties().getHandlingSpecifications().entrySet()) {
				//Add handling specifications
				deXML += "<Spec key=\"" + entry.getKey() + "\" value=\"" + entry.getValue() + "\"/>";
			}
			deXML += "</HandlingSpecs>";
		}
		deXML += "<Payload id=\""+this.messageProperties.getBusinessPayloadId()+
				"\">"+this.getBusinessPayload()+"</Payload>";
		if (this.addressed){
			deXML += "<Addressed/>";
		}
		deXML += "</ITKMessage>";
		Logger.trace("DEXML:"+deXML);
		fullPayload = TransformManager.doTransform("ToDistributionEnvelope.xslt", deXML);

		return fullPayload;
		
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessageImpl#setBusinessPayload(java.lang.String)
	 */
	public void setBusinessPayload(String businessPayload) {
		if (businessPayload != null) {
			if (businessPayload.startsWith("<?xml")) {
				//Remove any XML declarations
				this.businessPayload = businessPayload.substring(businessPayload.indexOf(">")+1);
			} else {
				this.businessPayload = businessPayload;
			}
			
		}
	}
}
