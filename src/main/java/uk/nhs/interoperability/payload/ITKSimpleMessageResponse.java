/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payload;

import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.transport.ITKTransportRoute;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class ITKSimpleMessageResponse.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ITKSimpleMessageResponse extends ITKMessageImpl {
	
	/**
	 * Instantiates a new iTK simple message response.
	 *
	 * @param itkMessageProperties the itk message properties
	 * @param reflectMessageProperties the reflect message properties
	 */
	public ITKSimpleMessageResponse(ITKMessageProperties itkMessageProperties, boolean reflectMessageProperties) {
		super(itkMessageProperties, null, null, reflectMessageProperties);
		/*
		 * Simple message responses do not have Distribution Envelope Wrappers
		 * applied when being transmitted. If there is a pre-resolved route
		 * (e.g. for the simple Async pattern) then make sure that no wrapper
		 * is specified
		 */
		if (super.getPreresolvedRoute() != null) {
			Logger.trace("Override DE wrapper - setting NO_WRAPPER");
			super.getPreresolvedRoute().setWrapperType(ITKTransportRoute.NO_WRAPPER);
		}
	}


	/**
	 * Instantiates a new iTK simple message response.
	 */
	public ITKSimpleMessageResponse() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.payload.ITKMessage#getFullPayload()
	 */
	@Override
	public String getFullPayload() {
		return "<itk:SimpleMessageResponse xmlns:itk=\"urn:nhs-itk:ns:201005\">OK</itk:SimpleMessageResponse>";
	}

}
