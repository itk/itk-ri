<?xml version="1.0" encoding="UTF-8"?>
<!-- 
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->
<xsl:stylesheet version="2.0"
	    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	    xmlns:xs="http://www.w3.org/2001/XMLSchema"
	    xmlns:fn="http://www.w3.org/2005/xpath-functions"
	    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" 
	    xmlns:wsa="http://www.w3.org/2005/08/addressing" 
	    xmlns:itk="urn:nhs-itk:ns:201005"
	    exclude-result-prefixes="xs fn">
	    
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/">

<soap:Envelope>
	<soap:Header>
		<wsa:MessageID><xsl:value-of select="SOAPMessage/MessageId"/></wsa:MessageID>
		<wsa:Action>http://www.w3.org/2005/08/addressing/fault</wsa:Action>
		<xsl:if test="SOAPMessage/To">
		  <wsa:To><xsl:value-of select="SOAPMessage/To"/></wsa:To>
		</xsl:if>
		<xsl:if test="SOAPMessage/From">
          <wsa:From><wsa:Address><xsl:value-of select="SOAPMessage/From"/></wsa:Address></wsa:From>
        </xsl:if>
        <xsl:if test="SOAPMessage/RelatesTo">
          <wsa:RelatesTo><xsl:value-of select="SOAPMessage/RelatesTo"/></wsa:RelatesTo>
        </xsl:if>
	</soap:Header>
	<soap:Body>
		<xsl:apply-templates select="SOAPMessage/FaultDetail"/>
	</soap:Body>
</soap:Envelope>

	</xsl:template>
	
	<xsl:template match="FaultDetail">
	   <soap:Fault>
	       <faultcode><xsl:value-of select="FaultLocation"/></faultcode>
	       <faultstring>A <xsl:value-of select="FaultLocation"/> related error has occurred, see detail element for further information</faultstring>
	       <xsl:if test="FaultActor">
	           <faultactor><xsl:value-of select="FaultActor"/></faultactor>
	       </xsl:if>
	       <detail>
	           <itk:ToolkitErrorInfo>
		           <itk:ErrorID><xsl:value-of select="ErrorID"/></itk:ErrorID>
		           <itk:ErrorCode>
		              <xsl:if test="ErrorCode/@codeSystem">
		                  <xsl:attribute name="codeSystem"><xsl:value-of select="ErrorCode/@codeSystem"/></xsl:attribute>
		              </xsl:if>		              
		              <xsl:value-of select="ErrorCode"/>
		           </itk:ErrorCode>
		           <itk:ErrorText><xsl:value-of select="ErrorText"/></itk:ErrorText>
		           <itk:ErrorDiagnosticText><xsl:value-of select="ErrorDiagnosticText"/></itk:ErrorDiagnosticText>
	           </itk:ToolkitErrorInfo>
	       </detail>
	     </soap:Fault>
	</xsl:template>
</xsl:stylesheet>
	