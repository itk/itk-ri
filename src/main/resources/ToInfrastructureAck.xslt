<?xml version="1.0" encoding="UTF-8"?>
<!-- 
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:itk="urn:nhs-itk:ns:201005"
    exclude-result-prefixes="xs">
    
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	
	<!-- xsl:variable name="result" select="if (/ITKInfraAck/FaultDetail) then 'Failure' else 'OK'"/ -->
	<xsl:variable name="result">
		<xsl:choose>
			<xsl:when test="/ITKInfraAck/FaultDetail">Failure</xsl:when>
			<xsl:otherwise>OK</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:template match="/">	
	   <itk:InfrastructureResponse timestamp="{ITKInfraAck/Created}" serviceRef="{ITKInfraAck/RefToService}" trackingIdRef="{ITKInfraAck/RefToTrackingId}" result="{$result}">
                <itk:reportingIdentity>
                    <itk:id uri="{ITKInfraAck/AuditId}"/>
                </itk:reportingIdentity>
                <!-- Create fault detail if it is a Nack -->
                <xsl:apply-templates select="ITKInfraAck/FaultDetail"/>
            </itk:InfrastructureResponse>
	</xsl:template>
	
	<xsl:template match="FaultDetail">
	   <itk:errors>
             <itk:errorInfo>
                 <itk:ErrorID><xsl:value-of select="ErrorID"/></itk:ErrorID>
                 <itk:ErrorCode>
                      <xsl:if test="ErrorCode/@codeSystem">
                          <xsl:attribute name="codeSystem"><xsl:value-of select="ErrorCode/@codeSystem"/></xsl:attribute>
                      </xsl:if>                   
                      <xsl:value-of select="ErrorCode"/></itk:ErrorCode>
                 <itk:ErrorText><xsl:value-of select="ErrorID"/></itk:ErrorText>
                 <itk:ErrorDiagnosticText><xsl:value-of select="ErrorID"/></itk:ErrorDiagnosticText>
             </itk:errorInfo>
         </itk:errors>
	</xsl:template>
	
</xsl:stylesheet>
